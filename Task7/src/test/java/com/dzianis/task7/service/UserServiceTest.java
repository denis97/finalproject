package com.dzianis.task7.service;

import com.dzianis.task7.domain.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Denis on 05.08.2017.
 */
public class UserServiceTest {
    private static User testUser;
    private UserService userService = new UserService();

    @Before
    public void create() throws Exception {
        testUser = new User();
        testUser.setLogin("testUser");
        testUser.setPassword("Test4585");
        testUser.setEmail("testUser@sportsBetSite.com");
        testUser.setFirstName("FirstName");
        testUser.setLastName("LastName");
        testUser.setAvatar("avatar.png");
        testUser = userService.create(testUser);
        assertNotNull(testUser.getId());
    }
    @After
    public void deleteById() throws Exception {
        assertTrue(userService.deleteById(testUser.getId()));
    }

    @Test
    public void findById() throws Exception {
        User usr = userService.findById(testUser.getId());
        assertNotNull(usr.getLogin());
        testUser = usr;
    }

    @Test
    public void update() throws Exception {
        User usr = userService.findById(testUser.getId());
        usr.setFirstName("NewFirstName");
        testUser = userService.update(usr);
        assertEquals(testUser.getFirstName(), "NewFirstName");
    }

    @Test
    public void findByLoginAndPassword() throws Exception {
        User usr = userService.findByLoginAndPassword(testUser.getLogin(), "Test4585");
        assertEquals(usr.getLogin(), testUser.getLogin());
    }


}