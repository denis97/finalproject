package com.dzianis.task7.service;

import com.dzianis.task7.domain.Competition;
import com.dzianis.task7.domain.RoleType;
import com.dzianis.task7.domain.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Denis on 05.08.2017.
 */
public class CompetitionServiceTest {
    private static User testUser;
    private static Competition com;
    private CompetitionService competitionService = new CompetitionService();
    private UserService userService = new UserService();
    @Before
    public void create() throws Exception {
        createUser();
        com = new Competition();
        com.setIdGame(1);
        com.setDatetimeStart(new Date());
        com.setDatetimeEnd(new Date());
        com.setLeftSide("1_Command");
        com.setRightSide("2_Command");
        com.setLeftSideGoal(0);
        com.setRightSideGoal(0);
        com.setIdUser(testUser.getId());
        com=competitionService.create(com);
        assertNotNull(com.getId());
    }
    private void createUser() throws Exception{
        testUser = new User();
        testUser.setLogin("testUser");
        testUser.setPassword("Test4585");
        testUser.setEmail("testUser@sportsBetSite.com");
        testUser.setFirstName("FirstName");
        testUser.setLastName("LastName");
        testUser.setAvatar("avatar.png");
        testUser = userService.create(testUser);
        assertNotNull(testUser.getId());
        testUser.setRole(RoleType.ADMIN);
    }
    @After
    public void deleteById() throws Exception {
        assertTrue(competitionService.deleteById(com.getId()));
        userService.deleteById(testUser.getId());
    }

    @Test
    public void findById() throws Exception {
        Competition c = competitionService.findById(com.getId());
        assertEquals(c.getLeftSide(),com.getLeftSide());
    }


    @Test
    public void update() throws Exception {
        com.setLeftSide("NEW");
        Competition nCom = competitionService.update(com);
        assertEquals(nCom.getLeftSide(),com.getLeftSide());
    }


}