package com.dzianis.task7.util.resource;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Denis on 07.08.2017.
 */
public class MessageManagerTest {
    @Test
    public void getProperty() throws Exception {
        assertEquals(MessageManager.getProperty("msg.test"),"Test");
    }

}