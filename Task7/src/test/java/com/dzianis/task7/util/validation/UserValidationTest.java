package com.dzianis.task7.util.validation;

import com.dzianis.task7.domain.User;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Denis on 07.08.2017.
 */
public class UserValidationTest {
    @Test
    public void isValid() throws Exception {
        UserValidation userValidation = new UserValidation();
        assertTrue(userValidation.isValid(getTestUser()));
    }
    @Test
    public void isNotValid() throws Exception {
        UserValidation userValidation = new UserValidation();
        User usr =getTestUser();
        usr.setPassword("12345678");
        assertFalse(userValidation.isValid(usr));
    }
    public User getTestUser(){
        User testUser = new User();
        testUser.setLogin("testUser");
        testUser.setPassword("Test4585");
        testUser.setEmail("testUser@sportsBetSite.com");
        testUser.setFirstName("FirstName");
        testUser.setLastName("LastName");
        testUser.setAvatar("avatar.png");
        return testUser;
    }

}