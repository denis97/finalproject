package com.dzianis.task7.service;

import com.dzianis.task7.dao.BetDAO;
import com.dzianis.task7.domain.Bet;
import com.dzianis.task7.exception.DAOException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.util.dbconnection.ConnectionPool;
import com.dzianis.task7.util.dbconnection.ProxyConnection;
import com.dzianis.task7.util.resource.MessageManager;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Denis on 12.07.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>Service for work with Bets in database.</b>
 */
public class BetService extends AbstractService<Bet> {
    private static final String CREATE_BET_ERROR_MSG = "msg.bet.create.error";
    private static final String UPDATE_BET_ERROR_MSG = "msg.bet.update.error";
    private static final String DELETE_BET_ERROR_MSG = "msg.bet.delete.error";
    private static final String BET_FINDBY_ID_ERROR_MSG = "msg.bet.findbyid.error";
    private static final String BET_FINDBY_USERID_ERROR_MSG = "msg.bet.findbyuserid.error";
    private static final String BET_FINDBY_COMPETITION_ERROR_MSG = "msg.bet.findbycompetition.error";
    private static final String BET_FINDBY_USER_AND_COMPETITION_ERROR_MSG = "msg.bet.findbyuserandcompetition.error";

    /**
     * This method creates new bet in the database.
     * @return Created bet.
     * @param bet domain object with data to write to the database.
     */
    public Bet create(Bet bet) throws ServiceException, InterruptedException {

        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            BetDAO betDAO = new BetDAO(connection);
            Bet currentBet = new Bet();
            currentBet = betDAO.create(bet);
            return currentBet;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(CREATE_BET_ERROR_MSG), e);
        }
    }

    /**
     * This method return bet by ID from the database.
     * @return Found bet.
     * @param id ID of the competition to find.
     */
    public Bet findById(int id) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            BetDAO betDAO = new BetDAO(connection);

            Bet bet = betDAO.findById(id);

            return bet;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(BET_FINDBY_ID_ERROR_MSG), e);
        }
    }

    /**
     * This method delete the bet in the database.
     * @return True if success.
     * @param id ID of the bet to delete.
     */
    public boolean deleteById(int id) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            BetDAO betDAO = new BetDAO(connection);
            return betDAO.deleteById(id);
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(DELETE_BET_ERROR_MSG), e);
        }
    }

    /**
     * This method updates the bet in the database.
     * @return Updated bet.
     * @param bet Bet with new data to update in database.
     */
    public Bet update(Bet bet) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            BetDAO betDAO = new BetDAO(connection);
            Bet currentBet = new Bet();
            currentBet = betDAO.update(bet);

            return currentBet;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(UPDATE_BET_ERROR_MSG), e);
        }
    }

    /**
     * This method returns a list of bets made by the user to the competition.
     * @return list of bets.
     * @param userId User ID.
     * @param competitionId Competition ID.
     */
    public List<Bet> findByUserAndCompetition(int userId, int competitionId) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            BetDAO betDAO = new BetDAO(connection);

            List<Bet> bets = betDAO.findByUserAndCompetition(userId, competitionId);

            return bets;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(BET_FINDBY_USER_AND_COMPETITION_ERROR_MSG), e);
        }
    }

    /**
     * This method returns a list of bets made by the user.
     * @return list of bets.
     * @param id User ID.
     */
    public List<Bet> findByUser(int id) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            BetDAO betDAO = new BetDAO(connection);

            List<Bet> bets = betDAO.findByUserId(id);

            return bets;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(BET_FINDBY_USERID_ERROR_MSG), e);
        }
    }

    /**
     * This method returns a list of bets made by the competition.
     * @return list of bets.
     * @param id Competition ID.
     */
    public List<Bet> findByCompetition(int id) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            BetDAO betDAO = new BetDAO(connection);

            List<Bet> bets = betDAO.findByCompetitionId(id);

            return bets;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(BET_FINDBY_COMPETITION_ERROR_MSG), e);
        }
    }
}
