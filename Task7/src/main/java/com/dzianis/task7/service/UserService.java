package com.dzianis.task7.service;


import com.dzianis.task7.dao.BetDAO;
import com.dzianis.task7.dao.UserDAO;
import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.DAOException;
import com.dzianis.task7.exception.NoSuchPageException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.util.dbconnection.ConnectionPool;
import com.dzianis.task7.util.dbconnection.ProxyConnection;
import com.dzianis.task7.util.resource.MessageManager;
import com.dzianis.task7.util.validation.UserValidation;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.xml.bind.ValidationException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Created by Denis on 17.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>Service for work with Users in database.</b>
 */
public class UserService extends AbstractService<User> {
    private static final String CREATE_USER_ERROR_MSG = "msg.create.user.error";
    private static final String FIND_USER_ERROR_MSG = "msg.find.user.error";
    private static final String DELETE_USER_ERROR_MSG = "msg.delete.user.error";
    private static final String UPDATE_USER_ERROR_MSG = "msg.update.user.error";
    private static final String USER_SEARCH_ERROR_MSG = "msg.search.user.error";
    private static final String USER_SEL_ALL_ERROR_MSG = "msg.selall.user.error";

    private static final Logger logger = LogManager.getLogger(UserService.class);
    private UserValidation userValidation = new UserValidation();

    /**
     * This method validate request and if valid then creates new user in the database.
     * @return Created user.
     * @param user domain object with data to write to the database. This object will stored to database only after validation.
     */
    public User create(User user) throws ServiceException, InterruptedException, ValidationException {

        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            UserDAO userDAO = new UserDAO(connection);
            User currentUser = new User();
            if (userValidation.isValid(user)) {
                currentUser = userDAO.create(user);
            } else {
                currentUser.setValidationExceptions(userValidation.getValidationExceptions());
            }
            return currentUser;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(CREATE_USER_ERROR_MSG), e);
        }
    }
    /**
     * This method return User by ID from the database.
     * @return Found user.
     * @param id ID of the user to find.
     */
    public User findById(int id) throws ServiceException, InterruptedException, NoSuchPageException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            UserDAO userDAO = new UserDAO(connection);
            User user = Optional.ofNullable(userDAO.findById(id))
                    .orElseThrow(() -> new NoSuchPageException(MessageManager.getProperty(FIND_USER_ERROR_MSG)));
            BetDAO betDAO = new BetDAO(connection);
            user.setBets(betDAO.findByUserId(id));
            return user;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(FIND_USER_ERROR_MSG), e);
        }
    }
    /**
     * This method updates the user in the database.
     * @return Updated user.
     * @param user user with new data to update in database.
     */
    public User update(User user) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            UserDAO userDAO = new UserDAO(connection);
            User currentUser = new User();
            currentUser = userDAO.update(user);
            return currentUser;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(UPDATE_USER_ERROR_MSG), e);
        }
    }
    /**
     * This method delete the user in the database.
     * @return True if success.
     * @param id ID of the user to delete.
     */
    public boolean deleteById(int id) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            UserDAO userDAO = new UserDAO(connection);
            return userDAO.deleteById(id);
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(DELETE_USER_ERROR_MSG), e);
        }
    }

    /**
     * This method used in login form.
     * @return User if login success.
     * @param login Username.
     * @param password user password in text format.
     */
    public User findByLoginAndPassword(String login, String password) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            UserDAO userDAO = new UserDAO(connection);
            return userDAO.findByLoginAndPassword(login, password);
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(FIND_USER_ERROR_MSG), e);
        }
    }
    /**
     * This method used in registration form to check if username is free for registration.
     * @return User if login exists.
     * @param login Username.
     */
    public User findByLogin(String login) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            UserDAO userDAO = new UserDAO(connection);
            return userDAO.findByLogin(login);
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(FIND_USER_ERROR_MSG), e);
        }
    }
    /**
     * This method used in find user command for search user by username or firstname or lastname.
     * @return User if found.
     * @param searchReq text witch will copared with username or firstname or lastname.
     */
    public List<User> searchUser(String searchReq) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            UserDAO userDAO = new UserDAO(connection);
            List<User> users = userDAO.searchUser(searchReq);
            return users;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(USER_SEARCH_ERROR_MSG), e);
        }
    }
    /**
     * This method used in find user command and returns all registered users.
     * @return All Users.
     */
    public List<User> getAll() throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            UserDAO userDAO = new UserDAO(connection);
            List<User> users = userDAO.getAll();
            return users;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(USER_SEL_ALL_ERROR_MSG), e);
        }
    }
}
