package com.dzianis.task7.service;


import com.dzianis.task7.dao.CompetitionDAO;
import com.dzianis.task7.domain.Competition;
import com.dzianis.task7.exception.DAOException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.util.dbconnection.ConnectionPool;
import com.dzianis.task7.util.dbconnection.ProxyConnection;
import com.dzianis.task7.util.resource.MessageManager;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Denis on 12.07.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>Service for work with Competitions in database.</b>
 */
public class CompetitionService extends AbstractService<Competition> {
    private static final String CREATE_COMPETITION_ERROR_MSG = "msg.competition.create.error";
    private static final String UPDATE_COMPETITION_ERROR_MSG = "msg.competition.update.error";
    private static final String DELETE_COMPETITION_ERROR_MSG = "msg.competition.delete.error";
    private static final String COMPETITION_FINDBY_ID_ERROR_MSG = "msg.competition.findbyid.error";
    private static final String COMPETITION_GET_UPCOMING_ERROR_MSG = "msg.competition.getupcoming.error";
    private static final String COMPETITION_FINDBY_GAME_ERROR_MSG = "msg.competition.findbygame.error";
    private static final String COMPETITION_GET_LIVE_ERROR_MSG = "msg.competition.getlive.error";
    private static final String COMPETITION_GET_TOP_ERROR_MSG = "msg.competition.gettop.error";
    private static final String COMPETITION_GET_NEXT_ERROR_MSG = "msg.competition.getnext.error";

    /**
     * This method creates new competition in the database.
     * @return Created competition.
     * @param competition domain object with data to write to the database.
     */
    public Competition create(Competition competition) throws ServiceException, InterruptedException {

        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CompetitionDAO comDAO = new CompetitionDAO(connection);
            Competition currentCompetition = new Competition();
            currentCompetition = comDAO.create(competition);
            return currentCompetition;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(CREATE_COMPETITION_ERROR_MSG), e);
        }
    }

    /**
     * This method return competition by ID from the database.
     * @return Found competition.
     * @param id ID of the competition to find.
     */
    public Competition findById(int id) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CompetitionDAO comDAO = new CompetitionDAO(connection);

            Competition com = comDAO.findById(id);

            return com;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(COMPETITION_FINDBY_ID_ERROR_MSG), e);
        }
    }
    /**
     * This method delete the competition in the database.
     * @return True if success.
     * @param id ID of the competition to delete.
     */
    public boolean deleteById(int id) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CompetitionDAO comDAO = new CompetitionDAO(connection);
            return comDAO.deleteById(id);
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(DELETE_COMPETITION_ERROR_MSG), e);
        }
    }
    /**
     * This method updates the competition in the database.
     * @return Updated Competition.
     * @param com Competition with new data to update in database.
     */
    public Competition update(Competition com) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CompetitionDAO comDAO = new CompetitionDAO(connection);
            Competition currentCom = new Competition();
            currentCom = comDAO.update(com);

            return currentCom;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(UPDATE_COMPETITION_ERROR_MSG), e);
        }
    }

    /**
     * @return All competitions for selected game.
     *
     * @param id The game ID for which the competition is requested
     * @param isAdmin If true then method returns all competitions for game, If false then returns only not started competitions
     */
    public List<Competition> findByGameId(int id, boolean isAdmin, int start, int end) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CompetitionDAO competitionDAO = new CompetitionDAO(connection);

            List<Competition> competitions = competitionDAO.findByGameId(id,isAdmin, start, end);

            return competitions;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(COMPETITION_FINDBY_GAME_ERROR_MSG), e);
        }
    }
    /**
     * @return Return selected number of competitions sorted by datetimeStart field.
     *
     * @param count Number of competitions that will return.
     */
    public List<Competition> getUpcomingGames(int count) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CompetitionDAO competitionDAO = new CompetitionDAO(connection);

            List<Competition> competitions = competitionDAO.getUpcomingGames(count);

            return competitions;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(COMPETITION_GET_UPCOMING_ERROR_MSG), e);
        }
    }
    /**
     * @return Returns the live competitions.
     */
    public List<Competition> getLiveGames() throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CompetitionDAO competitionDAO = new CompetitionDAO(connection);

            List<Competition> competitions = competitionDAO.getLiveGames();

            return competitions;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(COMPETITION_GET_LIVE_ERROR_MSG), e);
        }
    }
    /**
     * @return Returns the specified number of competitions in the order of the bets placed on them.
     *
     * @param size Number of competitions that will return.
     */
    public List<Competition> getTopGames(int size) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CompetitionDAO competitionDAO = new CompetitionDAO(connection);

            List<Competition> competitions = competitionDAO.getTopGames(size);

            return competitions;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(COMPETITION_GET_TOP_ERROR_MSG), e);
        }
    }
    /**
     * @return Returns the next (by time) competition.
     */
    public Competition getNextGame() throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CompetitionDAO competitionDAO = new CompetitionDAO(connection);

            Competition competition = competitionDAO.getNextGame();

            return competition;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(COMPETITION_GET_NEXT_ERROR_MSG), e);
        }
    }

    /**
     *
     * @param game ID GAME for which competitions will return
     * @param admin if true then returns all competitions (include started and ended)
     * @return Number of founded competitions
     */
    public int getCountByGame(int game, boolean admin) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CompetitionDAO competitionDAO = new CompetitionDAO(connection);

            return competitionDAO.getCountByGame(game,admin);

        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(COMPETITION_GET_NEXT_ERROR_MSG), e);
        }
    }
}
