package com.dzianis.task7.service;

import com.dzianis.task7.domain.Entity;
import com.dzianis.task7.exception.NoSuchPageException;
import com.dzianis.task7.exception.ServiceException;

import javax.xml.bind.ValidationException;


/**
 * Created by Denis on 17.06.2017.
 * This abstract class contains mandatory methods for the service level.
 */
public abstract class AbstractService<T extends Entity> {

    public abstract T create(T entity) throws ServiceException, InterruptedException, ValidationException;

    public abstract T findById(int id) throws ServiceException, InterruptedException, NoSuchPageException;

    public abstract T update(T entity) throws ServiceException, InterruptedException, ValidationException;

    public abstract boolean deleteById(int id) throws ServiceException, InterruptedException;
}
