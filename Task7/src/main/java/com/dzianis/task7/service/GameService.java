package com.dzianis.task7.service;

import com.dzianis.task7.dao.GameDAO;
import com.dzianis.task7.domain.Game;
import com.dzianis.task7.exception.DAOException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.util.dbconnection.ConnectionPool;
import com.dzianis.task7.util.dbconnection.ProxyConnection;
import com.dzianis.task7.util.resource.MessageManager;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Denis on 12.07.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>Service for work with Games in database.</b>
 */
public class GameService extends AbstractService<Game> {
    private static final String CREATE_GAME_ERROR_MSG = "msg.game.create.error";
    private static final String UPDATE_GAME_ERROR_MSG = "msg.game.update.error";
    private static final String DELETE_GAME_ERROR_MSG = "msg.game.delete.error";
    private static final String GAME_FINDBY_ID_ERROR_MSG = "msg.game.findbyid.error";
    private static final String GAME_GET_ALL_ERROR_MSG = "msg.game.getall.error";

    /**
     * This method creates new game in the database.
     * @return Created game.
     * @param game domain object with data to write to the database.
     */
    public Game create(Game game) throws ServiceException, InterruptedException {

        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            GameDAO gameDAO = new GameDAO(connection);
            Game currentCompetition = new Game();
            currentCompetition = gameDAO.create(game);
            return currentCompetition;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(CREATE_GAME_ERROR_MSG), e);
        }
    }
    /**
     * This method return Game by ID from the database.
     * @return Found game.
     * @param id ID of the game to find.
     */
    public Game findById(int id) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            GameDAO comDAO = new GameDAO(connection);
            Game com = comDAO.findById(id);
            return com;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(GAME_FINDBY_ID_ERROR_MSG), e);
        }
    }

    /**
     * This method delete the game in the database.
     * @return True if success.
     * @param id ID of the game to delete.
     */
    public boolean deleteById(int id) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            GameDAO comDAO = new GameDAO(connection);
            return comDAO.deleteById(id);
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(DELETE_GAME_ERROR_MSG), e);
        }
    }

    /**
     * This method updates the game in the database.
     * @return Updated game.
     * @param game game with new data to update in database.
     */
    public Game update(Game game) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            GameDAO gameDAO = new GameDAO(connection);
            Game currentGame = new Game();
            currentGame = gameDAO.update(game);
            return currentGame;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(UPDATE_GAME_ERROR_MSG), e);
        }
    }

    /**
     * This method returns all games from the database. Used when edit/create competition and in the left menu.
     * @return All games.
     */
    public List<Game> getAllGames() throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            GameDAO gameDAO = new GameDAO(connection);

            List<Game> games = gameDAO.getAllGames();

            return games;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(GAME_GET_ALL_ERROR_MSG), e);
        }
    }

}
