package com.dzianis.task7.service;

import com.dzianis.task7.dao.CoefficientDAO;
import com.dzianis.task7.domain.Coefficient;
import com.dzianis.task7.exception.DAOException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.util.dbconnection.ConnectionPool;
import com.dzianis.task7.util.dbconnection.ProxyConnection;
import com.dzianis.task7.util.resource.MessageManager;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Denis on 12.07.2017.
 *
 * @author Dzianis Hrydziushka
 * @version 1.0
 *          <b>Service for work with Coefficients in database.</b>
 */
public class CoefficientService extends AbstractService<Coefficient> {
    private static final String CREATE_COEFFICIENT_ERROR_MSG = "msg.coefficient.create.error";
    private static final String UPDATE_COEFFICIENT_ERROR_MSG = "msg.coefficient.update.error";
    private static final String DELETE_COEFFICIENT_ERROR_MSG = "msg.coefficient.delete.error";
    private static final String COEFFICIENT_FINDBY_ID_ERROR_MSG = "msg.coefficient.findbyid.error";
    private static final String COEFFICIENT_SET_FOR_COMPETITION_ERROR_MSG = "msg.coefficient.setforcompetition.error";
    private static final String COEFFICIENT_FINDBY_COMPETITION_ERROR_MSG = "msg.coefficient.findbycompetition.error";
    private static final String COEFFICIENT_FINDBY_COMPETITION_AND_BETTYPE_ERROR_MSG = "msg.coefficient.findbycompetitionandbettype.error";

    /**
     * This method return coefficient by Competition ID and Bet Type ID from the database.
     *
     * @param idCom     Competition ID.
     * @param idBetType Bet type ID.
     * @return Found coefficient.
     */
    public Coefficient findByComAndType(int idCom, int idBetType) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CoefficientDAO coefficientDAO = new CoefficientDAO(connection);
            Coefficient coefficient = coefficientDAO.findByComAndType(idCom, idBetType);
            return coefficient;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(COEFFICIENT_FINDBY_COMPETITION_AND_BETTYPE_ERROR_MSG), e);
        }
    }

    /**
     * This method return all coefficient by Competition ID from the database.
     *
     * @param id Competition ID.
     * @return List found coefficient.
     */
    public List<Coefficient> getCoefficientsForCompetition(int id) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CoefficientDAO coefficientDAO = new CoefficientDAO(connection);

            List<Coefficient> coefficients = coefficientDAO.getCoefficientsForCompetition(id);

            return coefficients;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(COEFFICIENT_FINDBY_COMPETITION_ERROR_MSG), e);
        }
    }

    /**
     * This method set coefficient for Competition and Bet type.
     *
     * @param idCompetition Competition ID.
     * @param idBetType     Bet type ID.
     * @param coefficient   Coefficient value.
     */
    public void setCoefficientForCompetition(int idCompetition, int idBetType, BigDecimal coefficient) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CoefficientDAO comDAO = new CoefficientDAO(connection);
            comDAO.setCoefficientForCompetition(idCompetition, idBetType, coefficient);
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(COEFFICIENT_SET_FOR_COMPETITION_ERROR_MSG), e);
        }
    }

    /**
     * This method creates new coefficient in the database.
     *
     * @param coefficient domain object with data to write to the database.
     * @return Created coefficient.
     * @deprecated This method is not needed and not working.
     */
    public Coefficient create(Coefficient coefficient) throws ServiceException, InterruptedException {

        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CoefficientDAO comDAO = new CoefficientDAO(connection);
            Coefficient currentCoefficient = new Coefficient();
            currentCoefficient = comDAO.create(coefficient);
            return currentCoefficient;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(CREATE_COEFFICIENT_ERROR_MSG), e);
        }
    }

    /**
     * This method return coefficient by ID from the database.
     *
     * @param id ID of the coefficient to find.
     * @return Found coefficient.
     * @deprecated This method is not needed and not working.
     */
    public Coefficient findById(int id) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CoefficientDAO comDAO = new CoefficientDAO(connection);

            Coefficient com = comDAO.findById(id);

            return com;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(COEFFICIENT_FINDBY_ID_ERROR_MSG), e);
        }
    }

    /**
     * This method delete the coefficient in the database.
     *
     * @param id ID of the coefficient to delete.
     * @return True if success.
     * @deprecated This method is not needed and not working.
     */
    public boolean deleteById(int id) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CoefficientDAO comDAO = new CoefficientDAO(connection);
            return comDAO.deleteById(id);
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(DELETE_COEFFICIENT_ERROR_MSG), e);
        }
    }

    /**
     * This method updates the coefficient in the database.
     *
     * @param com Coefficient with new data to update in database.
     * @return Updated Coefficient.
     * @deprecated This method is not needed and not working.
     */
    public Coefficient update(Coefficient com) throws ServiceException, InterruptedException {
        try (ProxyConnection connection = ConnectionPool.getInstance().getConnection()) {
            CoefficientDAO comDAO = new CoefficientDAO(connection);
            Coefficient currentCom = new Coefficient();
            currentCom = comDAO.update(com);

            return currentCom;
        } catch (DAOException | SQLException e) {
            throw new ServiceException(MessageManager.getProperty(UPDATE_COEFFICIENT_ERROR_MSG), e);
        }
    }


}
