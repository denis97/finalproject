package com.dzianis.task7.dao;

import com.dzianis.task7.domain.RoleType;
import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.DAOException;
import com.dzianis.task7.util.dbconnection.ProxyConnection;
import com.dzianis.task7.util.resource.MessageManager;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


import static com.dzianis.task7.util.dbconnection.query.SQLUserQuery.*;

/**
 * Created by Denis on 17.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 */
public class UserDAO extends AbstractDAO<User> {

    private static final String ID = "idUsers";
    private static final String ROLE = "User_groups_idUser_groups";
    private static final String LOGIN = "username";
    private static final String PASSWORD = "passwordHash";
    private static final String EMAIL = "email";
    private static final String IS_ACTIVE = "isActive";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "lastname";
    private static final String MONEY = "money_count";
    private static final String AVATAR = "avatar";
    private static final String CREATE_USER_ERROR_MSG = "msg.create.user.error";
    private static final String FIND_USER_ERROR_MSG = "msg.find.user.error";
    private static final String DELETE_USER_ERROR_MSG = "msg.delete.user.error";
    private static final String UPDATE_USER_ERROR_MSG = "msg.update.user.error";
    private static final String UPDATE_USER_RATING_ERROR_MSG = "msg.update.user.rating.error";
    private static final String USER_SEARCH_ERROR_MSG = "msg.search.user.error";
    private static final String USER_SEL_ALL_ERROR_MSG = "msg.selall.user.error";

    private static final Logger logger = LogManager.getLogger(UserDAO.class);

    public UserDAO(ProxyConnection connection) {
        super(connection);
    }

    public User create(User user) throws DAOException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, DigestUtils.sha256Hex(user.getPassword())); //hashing password
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getFirstName());
            preparedStatement.setString(5, user.getLastName());
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                user.setId(generatedKeys.getInt(1));
            }
            return user;
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DAOException(MessageManager.getProperty(CREATE_USER_ERROR_MSG), e);
        }
    }

    public User findById(int id) throws DAOException {
        User user = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user = createUserFromResultSet(resultSet);
            }
            return user;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(FIND_USER_ERROR_MSG), e);
        }
    }

    public User update(User user) throws DAOException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_USER)) {
            preparedStatement.setString(1, user.getEmail());
            preparedStatement.setString(2, user.getFirstName());
            preparedStatement.setString(3, user.getLastName());
            preparedStatement.setInt(4, user.getMoney());
            preparedStatement.setString(5, user.getAvatar());
            preparedStatement.setString(6, user.getPassword());
            preparedStatement.setBoolean(7, user.isActive());
            preparedStatement.setInt(8, user.getRole().ordinal()+1);
            preparedStatement.setInt(9, user.getId());
            preparedStatement.executeUpdate();
            return user;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(UPDATE_USER_ERROR_MSG), e);
        }
    }

    public boolean deleteById(int id) throws DAOException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_USER_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(DELETE_USER_ERROR_MSG), e);
        }
    }

    public User findByLoginAndPassword(String login, String password) throws DAOException {
        User user = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_LOGIN_AND_PASSWORD)) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, DigestUtils.sha256Hex(password));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user = createUserFromResultSet(resultSet);
            }
            return user;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(FIND_USER_ERROR_MSG), e);
        }
    }
    public User findByLogin(String login) throws DAOException {
        User user = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_LOGIN)) {
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user = createUserFromResultSet(resultSet);
            }
            return user;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(FIND_USER_ERROR_MSG), e);
        }
    }

    public void updateUserMoney(int userId, int newMoney) throws DAOException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_USER_MONEY)) {
            preparedStatement.setInt(1, newMoney);
            preparedStatement.setInt(2, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(UPDATE_USER_RATING_ERROR_MSG), e);
        }
    }

    public List<User> searchUser(String searchReq) throws DAOException {
        List<User> users = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SEARCH_USER)) {
            preparedStatement.setString(1, "%" + searchReq + "%");
            preparedStatement.setString(2, "%" + searchReq + "%");
            preparedStatement.setString(3, "%" + searchReq + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = createUserFromResultSet(resultSet);
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(USER_SEARCH_ERROR_MSG), e);
        }
    }
    public List<User> getAll() throws DAOException {
        List<User> users = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ALL_USER)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = createUserFromResultSet(resultSet);
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(USER_SEL_ALL_ERROR_MSG), e);
        }
    }

    private User createUserFromResultSet(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt(ID));
        user.setRole(RoleType.values()[resultSet.getInt(ROLE) - 1]);
        user.setLogin(resultSet.getString(LOGIN));
        user.setPassword(resultSet.getString(PASSWORD));
        user.setEmail(resultSet.getString(EMAIL));
        user.setActive(resultSet.getBoolean(IS_ACTIVE));
        user.setFirstName(resultSet.getString(FIRST_NAME));
        user.setLastName(resultSet.getString(LAST_NAME));
        user.setMoney(resultSet.getInt(MONEY));
        user.setAvatar(resultSet.getString(AVATAR));
        return user;
    }

}
