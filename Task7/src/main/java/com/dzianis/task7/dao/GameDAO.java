package com.dzianis.task7.dao;

/**
 * Created by Denis on 23.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 */

import com.dzianis.task7.domain.Game;
import com.dzianis.task7.exception.DAOException;
import com.dzianis.task7.util.dbconnection.ProxyConnection;
import com.dzianis.task7.util.resource.MessageManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.dzianis.task7.util.dbconnection.query.SQLGameQuery.SQL_SELECT_ALL_GAMES;
import static com.dzianis.task7.util.dbconnection.query.SQLGameQuery.SQL_SELECT_GAME_BY_ID;


public class GameDAO extends AbstractDAO<Game> {

    private static final String ID = "idGames";
    private static final String TITLE = "title";

    private static final String CREATE_GAME_ERROR_MSG = "msg.game.create.error";
    private static final String UPDATE_GAME_ERROR_MSG = "msg.game.update.error";
    private static final String DELETE_GAME_ERROR_MSG = "msg.game.delete.error";
    private static final String GAME_FINDBY_ID_ERROR_MSG = "msg.game.findbyid.error";
    private static final String GAME_GET_ALL_ERROR_MSG = "msg.game.getall.error";

    private static final Logger logger = LogManager.getLogger(GameDAO.class);

    public GameDAO(ProxyConnection connection) {
        super(connection);
    }

    public Game create(Game game) throws DAOException {
        return new Game();
    }

    public Game findById(int id) throws DAOException {
        Game game = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_GAME_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                game = createGameFromResultSet(resultSet);
            }
            return game;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(GAME_FINDBY_ID_ERROR_MSG), e);
        }
    }

    public Game update(Game game) throws DAOException {
        return new Game();
    }

    public boolean deleteById(int id) throws DAOException {
        return true;
    }

    public List<Game> getAllGames() throws DAOException {
        List<Game> games = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_GAMES)) {

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Game game = createGameFromResultSet(resultSet);
                games.add(game);
            }
            return games;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(GAME_GET_ALL_ERROR_MSG), e);
        }
    }


    private Game createGameFromResultSet(ResultSet resultSet) throws SQLException {
        Game game = new Game();
        game.setId(resultSet.getInt(ID));
        game.setTitle(resultSet.getString(TITLE));
        return game;
    }

}


