package com.dzianis.task7.dao;

import com.dzianis.task7.domain.Bet;
import com.dzianis.task7.domain.Coefficient;
import com.dzianis.task7.domain.Competition;
import com.dzianis.task7.exception.DAOException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.CoefficientService;
import com.dzianis.task7.service.CompetitionService;
import com.dzianis.task7.util.dbconnection.ProxyConnection;
import com.dzianis.task7.util.resource.MessageManager;
import com.mysql.jdbc.Statement;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static com.dzianis.task7.util.dbconnection.query.SQLBetQuery.*;


/**
 * Created by Denis on 17.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 */
public class BetDAO extends AbstractDAO<Bet> {

    private static final String ID = "idBets";
    private static final String MONEY = "money";
    private static final String DATETIME = "datetime";
    private static final String LEFT_GOAL = "leftSideGoal";
    private static final String RIGHT_GOAL = "rightSideGoal";
    private static final String WIN = "isWin";
    private static final String PROCESSED = "isProcessed";
    private static final String ID_BET_TYPE = "BetTypes_idBetTypes";
    private static final String ID_COMPETITION = "Competitions_idCompetitions";
    private static final String ID_USER = "Users_idUsers";

    private static final String CREATE_BET_ERROR_MSG = "msg.bet.create.error";
    private static final String UPDATE_BET_ERROR_MSG = "msg.bet.update.error";
    private static final String DELETE_BET_ERROR_MSG = "msg.bet.delete.error";
    private static final String BET_FINDBY_ID_ERROR_MSG = "msg.bet.findbyid.error";
    private static final String BET_FINDBY_USERID_ERROR_MSG = "msg.bet.findbyuserid.error";
    private static final String BET_FINDBY_COMPETITION_ERROR_MSG = "msg.bet.findbycompetition.error";
    private static final String BET_FINDBY_USER_AND_COMPETITION_ERROR_MSG = "msg.bet.findbyuserandcompetition.error";
    private static final String BET_GET_COMPETITION_ERROR_MSG = "msg.bet.getcompetition.error";
    private static final String BET_GET_COEFFICIENT_ERROR_MSG = "msg.bet.getcoefficient.error";


    private static final Logger logger = LogManager.getLogger(BetDAO.class);

    public BetDAO(ProxyConnection connection) {
        super(connection);
    }

    public Bet create(Bet bet) throws DAOException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_BET, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, bet.getMoney());
            preparedStatement.setTimestamp(2, new Timestamp(bet.getDatetime().getTime()));
            preparedStatement.setInt(3, bet.getLeftSideGoal());
            preparedStatement.setInt(4, bet.getRightSideGoal());
            preparedStatement.setInt(5, bet.getIdBetType());
            preparedStatement.setInt(6, bet.getIdCompetition());
            preparedStatement.setInt(7, bet.getIdUser());
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                bet.setId(generatedKeys.getInt(1));
            }
            return bet;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(CREATE_BET_ERROR_MSG), e);
        }
    }

    public Bet findById(int id) throws DAOException {
        Bet bet = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BET_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                bet = createBetFromResultSet(resultSet);
            }
            return bet;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(BET_FINDBY_ID_ERROR_MSG), e);
        }
    }

    public Bet update(Bet bet) throws DAOException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_BET)) {
            preparedStatement.setBoolean(1, bet.isWin());
            preparedStatement.setBoolean(2, bet.isProcessed());
            preparedStatement.setInt(3, bet.getId());
            preparedStatement.executeUpdate();
            return bet;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(UPDATE_BET_ERROR_MSG), e);
        }
    }

    public boolean deleteById(int id) throws DAOException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_BET_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(DELETE_BET_ERROR_MSG), e);
        }
    }

    public List<Bet> findByUserId(int id) throws DAOException {
        List<Bet> bets = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BET_BY_USER_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Bet bet = createBetFromResultSet(resultSet);
                bets.add(bet);
            }
            return bets;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(BET_FINDBY_USERID_ERROR_MSG), e);
        }
    }

    public List<Bet> findByCompetitionId(int id) throws DAOException {
        List<Bet> bets = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BET_BY_COMPETITION_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Bet bet = createBetFromResultSet(resultSet);
                bets.add(bet);
            }
            return bets;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(BET_FINDBY_COMPETITION_ERROR_MSG), e);
        }
    }

    public List<Bet> findByUserAndCompetition(int idUser, int idCompetition) throws DAOException {
        List<Bet> bets = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BET_BY_USR_AND_COM)) {
            preparedStatement.setInt(1, idUser);
            preparedStatement.setInt(2, idCompetition);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Bet bet = createBetFromResultSet(resultSet);
                bets.add(bet);
            }
            return bets;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(BET_FINDBY_USER_AND_COMPETITION_ERROR_MSG), e);
        }
    }

    private Bet createBetFromResultSet(ResultSet resultSet) throws SQLException {
        Bet bet = new Bet();
        bet.setId(resultSet.getInt(ID));
        bet.setMoney(resultSet.getInt(MONEY));
        bet.setDatetime(new java.util.Date(resultSet.getTimestamp(DATETIME).getTime()));
        bet.setIdCompetition(resultSet.getInt(ID_COMPETITION));
        bet.setIdBetType(resultSet.getInt(ID_BET_TYPE));
        bet.setLeftSideGoal(resultSet.getInt(LEFT_GOAL));
        bet.setRightSideGoal(resultSet.getInt(RIGHT_GOAL));
        bet.setIdUser((resultSet.getInt(ID_USER)));
        bet.setWin(resultSet.getBoolean(WIN));
        bet.setProcessed(resultSet.getBoolean(PROCESSED));
        bet.setCompetition(getCompetition(bet.getIdCompetition()));
        bet.setCoefficient(getCoefficient(bet.getIdCompetition(), bet.getIdBetType()));
        return bet;
    }

    private Competition getCompetition(int id) {
        CompetitionService competitionService = new CompetitionService();
        try {
            Competition competition = competitionService.findById(id);
            return competition;
        } catch (ServiceException | InterruptedException e) {
            logger.error(MessageManager.getProperty(BET_GET_COMPETITION_ERROR_MSG) + e.getMessage());
            return new Competition();
        }
    }

    private Coefficient getCoefficient(int idCom, int idBetType) {
        CoefficientService coefficientService = new CoefficientService();
        try {
            Coefficient coefficient = coefficientService.findByComAndType(idCom, idBetType);
            return coefficient;
        } catch (ServiceException | InterruptedException e) {
            logger.error(MessageManager.getProperty(BET_GET_COEFFICIENT_ERROR_MSG) + e.getMessage());
            return new Coefficient();
        }
    }
}

