package com.dzianis.task7.dao;

/**
 * Created by Denis on 23.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 */


import com.dzianis.task7.domain.Competition;
import com.dzianis.task7.domain.Game;
import com.dzianis.task7.exception.DAOException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.CoefficientService;
import com.dzianis.task7.service.CompetitionService;
import com.dzianis.task7.service.GameService;
import com.dzianis.task7.util.dbconnection.ProxyConnection;
import com.dzianis.task7.util.resource.MessageManager;
import com.mysql.jdbc.Statement;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.dzianis.task7.util.dbconnection.query.SQLCompetitionQuery.*;


public class CompetitionDAO extends AbstractDAO<Competition> {

    private static final String ID = "idCompetitions";
    private static final String LEFT_TITLE = "leftSide";
    private static final String RIGHT_TITLE = "rightSide";
    private static final String DATETIME_START = "datetime_start";
    private static final String DATETIME_END = "datetime_end";
    private static final String LEFT_GOAL = "leftSideGoal";
    private static final String RIGHT_GOAL = "rightSideGoal";
    private static final String PROCESSED = "isProcessed";
    private static final String ID_GAME = "Games_idGames";
    private static final String ID_USER = "Users_idUsers";
    private static final String CREATE_COMPETITION_ERROR_MSG = "msg.competition.create.error";
    private static final String UPDATE_COMPETITION_ERROR_MSG = "msg.competition.update.error";
    private static final String DELETE_COMPETITION_ERROR_MSG = "msg.competition.delete.error";
    private static final String COMPETITION_FINDBY_ID_ERROR_MSG = "msg.competition.findbyid.error";
    private static final String COMPETITION_GET_UPCOMING_ERROR_MSG = "msg.competition.getupcoming.error";
    private static final String COMPETITION_FINDBY_GAME_ERROR_MSG = "msg.competition.findbygame.error";
    private static final String COMPETITION_GET_LIVE_ERROR_MSG = "msg.competition.getlive.error";
    private static final String COMPETITION_GET_TOP_ERROR_MSG = "msg.competition.gettop.error";
    private static final String COMPETITION_GET_NEXT_ERROR_MSG = "msg.competition.getnext.error";
    private static final String COMPETITION_GET_COEFFICIENTS_ERROR_MSG = "msg.competition.getcoefficients.error";
    private static final String COMPETITION_GET_GAME_ERROR_MSG = "msg.competition.getgame.error";

    private CoefficientService coefficientService = new CoefficientService();
    private static final Logger logger = LogManager.getLogger(CompetitionDAO.class);

    public CompetitionDAO(ProxyConnection connection) {
        super(connection);
    }

    /**
     * @see com.dzianis.task7.service.CompetitionService#create(Competition)
     */
    public Competition create(Competition com) throws DAOException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_COMPETITION, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, com.getLeftSide());
            preparedStatement.setString(2, com.getRightSide());
            preparedStatement.setTimestamp(3, new Timestamp(com.getDatetimeStart().getTime()));
            preparedStatement.setTimestamp(4, new Timestamp(com.getDatetimeEnd().getTime()));
            preparedStatement.setInt(5, com.getLeftSideGoal());
            preparedStatement.setInt(6, com.getRightSideGoal());
            preparedStatement.setInt(7, com.getIdGame());
            preparedStatement.setInt(8, com.getIdUser());
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                com.setId(generatedKeys.getInt(1));
            }
            return com;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(CREATE_COMPETITION_ERROR_MSG), e);
        }
    }
    /**
     * @see com.dzianis.task7.service.CompetitionService#findById(int)
     */
    public Competition findById(int id) throws DAOException {
        Competition com = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_COMPETITION_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                com = createCompetitionFromResultSet(resultSet);
            }
            return com;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(COMPETITION_FINDBY_ID_ERROR_MSG), e);
        }
    }

    /**
     * @see com.dzianis.task7.service.CompetitionService#update(Competition)
     */
    public Competition update(Competition com) throws DAOException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_COMPETITION)) {
            preparedStatement.setString(1, com.getLeftSide());
            preparedStatement.setString(2, com.getRightSide());
            preparedStatement.setTimestamp(3, new Timestamp(com.getDatetimeStart().getTime()));
            preparedStatement.setTimestamp(4, new Timestamp(com.getDatetimeEnd().getTime()));
            preparedStatement.setInt(5, com.getLeftSideGoal());
            preparedStatement.setInt(6, com.getRightSideGoal());
            preparedStatement.setInt(7, com.getIdGame());
            preparedStatement.setInt(8, com.getIdUser());
            preparedStatement.setBoolean(9, com.isProcessed());
            preparedStatement.setInt(10, com.getId());
            preparedStatement.executeUpdate();
            return com;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(UPDATE_COMPETITION_ERROR_MSG), e);
        }
    }

    /**
     * @see com.dzianis.task7.service.CompetitionService#deleteById(int)
     */
    public boolean deleteById(int id) throws DAOException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_COMPETITION_BY_ID)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(DELETE_COMPETITION_ERROR_MSG), e);
        }
    }

    /**
     * @see com.dzianis.task7.service.CompetitionService#findByGameId(int, boolean)
     */
    public List<Competition> findByGameId(int id, boolean isAdmin,int start, int end) throws DAOException {
        List<Competition> competitions = new ArrayList<>();
        String sql = SQL_SELECT_COMPETITION_BY_GAME_ID;
        if (isAdmin)
            sql = SQL_SELECT_COMPETITION_BY_GAME_ID_ADMIN;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, start);
            preparedStatement.setInt(3, end);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Competition com = createCompetitionFromResultSet(resultSet);
                competitions.add(com);
            }
            return competitions;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(COMPETITION_FINDBY_GAME_ERROR_MSG), e);
        }
    }

    /**
     * @see com.dzianis.task7.service.CompetitionService#getUpcomingGames(int)
     */
    public List<Competition> getUpcomingGames(int count) throws DAOException {
        List<Competition> games = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_COMPETITION_UPCOMING)) {
            preparedStatement.setInt(1, count);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Competition game = createCompetitionFromResultSet(resultSet);
                games.add(game);
            }
            return games;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(COMPETITION_GET_UPCOMING_ERROR_MSG), e);
        }
    }

    /**
     * @see CompetitionService#getLiveGames()
     */
    public List<Competition> getLiveGames() throws DAOException {
        List<Competition> games = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_LIVE_COMPETITIONS)) {

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Competition game = createCompetitionFromResultSet(resultSet);
                games.add(game);
            }
            return games;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(COMPETITION_GET_LIVE_ERROR_MSG), e);
        }
    }

    /**
     * @see com.dzianis.task7.service.CompetitionService#getTopGames(int)
     */
    public List<Competition> getTopGames(int size) throws DAOException {
        List<Competition> games = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_TOP_COMPETITIONS)) {
            preparedStatement.setInt(1, size);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Competition game = createCompetitionFromResultSet(resultSet);
                games.add(game);
            }
            return games;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(COMPETITION_GET_TOP_ERROR_MSG), e);
        }
    }

    /**
     * @see CompetitionService#getNextGame()
     */
    public Competition getNextGame() throws DAOException {
        Competition com = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_NEXT_COMPETITION)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                com = createCompetitionFromResultSet(resultSet);
            }
            return com;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(COMPETITION_GET_NEXT_ERROR_MSG), e);
        }
    }

    /**
     *
     * @return Count competitions in game for paging
     * @throws DAOException
     */
    public int getCountByGame(int game, boolean admin) throws DAOException {
        String sql = SQL_COUNT_COMPETITIONS_BY_GAME_ID;
        if (admin)
            sql = SQL_COUNT_COMPETITIONS_BY_GAME_ID_ADMIN;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, game);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(COMPETITION_GET_NEXT_ERROR_MSG), e);
        }
        return 0;
    }

    /**
     * This method create new competition from result set data.
     * @param resultSet Result set from Statement
     * @return New competition from result set data
     * @throws SQLException If error in result set
     */
    private Competition createCompetitionFromResultSet(ResultSet resultSet) throws SQLException {
        Competition com = new Competition();
        com.setId(resultSet.getInt(ID));
        com.setLeftSide(resultSet.getString(LEFT_TITLE));
        com.setRightSide(resultSet.getString(RIGHT_TITLE));
        com.setDatetimeStart(new Date(resultSet.getTimestamp(DATETIME_START).getTime()));
        com.setDatetimeEnd(new Date(resultSet.getTimestamp(DATETIME_END).getTime()));
        com.setLeftSideGoal(resultSet.getInt(LEFT_GOAL));
        com.setRightSideGoal(resultSet.getInt(RIGHT_GOAL));
        com.setIdGame(resultSet.getInt(ID_GAME));
        com.setIdUser(resultSet.getInt(ID_USER));
        com.setProcessed(resultSet.getBoolean(PROCESSED));
        try {
            com.setCoefficients(coefficientService.getCoefficientsForCompetition(com.getId()));
        } catch (ServiceException | InterruptedException e) {
            logger.error(MessageManager.getProperty(COMPETITION_GET_COEFFICIENTS_ERROR_MSG) + e.getMessage());
        }
        GameService gameService = new GameService();
        try {
            Game game = gameService.findById(com.getIdGame());
            com.setGame(game);
        } catch (ServiceException | InterruptedException e) {
            logger.error(MessageManager.getProperty(COMPETITION_GET_GAME_ERROR_MSG) + e.getMessage());
        }

        return com;
    }

}


