package com.dzianis.task7.dao;


import com.dzianis.task7.domain.Entity;
import com.dzianis.task7.exception.DAOException;
import com.dzianis.task7.util.dbconnection.ProxyConnection;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * @author Dzianis Hrydziushka
 * @version 1.0
 * @param <T> damain type of data
 */

public abstract class AbstractDAO<T extends Entity> {
    private static final Logger logger = LogManager.getLogger(AbstractDAO.class);
    protected ProxyConnection connection;

    public AbstractDAO(ProxyConnection connection) {
        this.connection = connection;
    }

    public abstract T create(T entity) throws DAOException;

    public abstract T findById(int id) throws DAOException;

    public abstract T update(T entity) throws DAOException;

    public abstract boolean deleteById(int id) throws DAOException;


}
