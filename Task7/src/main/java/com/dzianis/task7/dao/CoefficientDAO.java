package com.dzianis.task7.dao;

/**
 * Created by Denis on 23.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 */

import com.dzianis.task7.domain.Coefficient;
import com.dzianis.task7.exception.DAOException;
import com.dzianis.task7.util.dbconnection.ProxyConnection;
import com.dzianis.task7.util.resource.MessageManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.dzianis.task7.util.dbconnection.query.SQLCoefficientQuery.*;


public class CoefficientDAO extends AbstractDAO<Coefficient> {

    private static final String ID_COMPETITION = "Competitions_idCompetitions";
    private static final String ID_BETTYPE = "BetTypes_idBetTypes";
    private static final String COEFFICIENT_VALUE = "coefficient";
    private static final String TITLE = "title";

    private static final String CREATE_COEFFICIENT_ERROR_MSG = "msg.coefficient.create.error";
    private static final String UPDATE_COEFFICIENT_ERROR_MSG = "msg.coefficient.update.error";
    private static final String DELETE_COEFFICIENT_ERROR_MSG = "msg.coefficient.delete.error";
    private static final String COEFFICIENT_FINDBY_ID_ERROR_MSG = "msg.coefficient.findbyid.error";
    private static final String COEFFICIENT_SET_FOR_COMPETITION_ERROR_MSG = "msg.coefficient.setforcompetition.error";
    private static final String COEFFICIENT_FINDBY_COMPETITION_ERROR_MSG = "msg.coefficient.findbycompetition.error";
    private static final String COEFFICIENT_FINDBY_COMPETITION_AND_BETTYPE_ERROR_MSG = "msg.coefficient.findbycompetitionandbettype.error";


    private static final Logger logger = LogManager.getLogger(CoefficientDAO.class);

    public CoefficientDAO(ProxyConnection connection) {
        super(connection);
    }

    public Coefficient create(Coefficient c) throws DAOException {
        return c;
    }

    public Coefficient findById(int id) throws DAOException {
        Coefficient c = null;
        return c;
    }

    public Coefficient update(Coefficient com) throws DAOException {
        return com;
    }

    public boolean deleteById(int id) throws DAOException {
        return true;
    }

    public Coefficient findByComAndType(int idCom, int idBetType) throws DAOException {
        Coefficient c = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_COEFFICIENT_BY_ID)) {
            preparedStatement.setInt(1, idCom);
            preparedStatement.setInt(2, idBetType);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                c = createCoefficientFromResultSet(resultSet);
            }
            return c;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(COEFFICIENT_FINDBY_COMPETITION_AND_BETTYPE_ERROR_MSG), e);
        }
    }

    public void setCoefficientForCompetition(int idCompetition, int idBetType, BigDecimal coefficient) throws DAOException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SET_COEFFICIENTS_FOR_COMPETITION)) {
            preparedStatement.setInt(1, idCompetition);
            preparedStatement.setInt(2, idBetType);
            preparedStatement.setBigDecimal(3, coefficient);
            preparedStatement.setBigDecimal(4, coefficient);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(COEFFICIENT_SET_FOR_COMPETITION_ERROR_MSG), e);
        }
    }

    public List<Coefficient> getCoefficientsForCompetition(int id) throws DAOException {
        List<Coefficient> coefficients = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_COEFFICIENT_FOR_COMPETITION)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Coefficient coefficient = createCoefficientFromResultSet(resultSet);
                coefficients.add(coefficient);
            }
            return coefficients;
        } catch (SQLException e) {
            throw new DAOException(MessageManager.getProperty(COEFFICIENT_FINDBY_COMPETITION_ERROR_MSG), e);
        }
    }

    private Coefficient createCoefficientFromResultSet(ResultSet resultSet) throws SQLException {
        Coefficient coefficient = new Coefficient();
        coefficient.setIdCompetition(resultSet.getInt(ID_COMPETITION));
        coefficient.setIdCoefficientType(resultSet.getInt(ID_BETTYPE));
        coefficient.setCoefficient(resultSet.getBigDecimal(COEFFICIENT_VALUE));
        coefficient.setTitle(resultSet.getString(TITLE));
        return coefficient;
    }

}


