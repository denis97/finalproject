package com.dzianis.task7.domain;

import java.math.BigDecimal;

/**
 * Created by Denis on 03.07.2017.
 */
public class Coefficient extends Entity {
    private int idCompetition;
    private int idCoefficientType;
    private String title;
    private BigDecimal coefficient;

    public int getIdCompetition() {
        return idCompetition;
    }

    public void setIdCompetition(int idCompetition) {
        this.idCompetition = idCompetition;
    }

    public int getIdCoefficientType() {
        return idCoefficientType;
    }

    public void setIdCoefficientType(int idCoefficientType) {
        this.idCoefficientType = idCoefficientType;
    }

    public BigDecimal getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(BigDecimal coefficient) {
        this.coefficient = coefficient;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
