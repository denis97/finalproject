package com.dzianis.task7.domain;


import java.util.Date;
import java.util.List;

/**
 * Created by Denis on 23.06.2017.
 */
public class Competition extends Entity {
    private int id;
    private String leftSide;
    private String rightSide;
    private Date datetimeStart;
    private Date datetimeEnd;
    private int leftSideGoal;
    private int rightSideGoal;
    private int idGame;
    private Game game;
    private int idUser;
    private boolean processed;
    private List<Coefficient> coefficients;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLeftSide() {
        return leftSide;
    }

    public void setLeftSide(String leftSide) {
        this.leftSide = leftSide;
    }

    public String getRightSide() {
        return rightSide;
    }

    public void setRightSide(String rightSide) {
        this.rightSide = rightSide;
    }

    public Date getDatetimeStart() {
        return datetimeStart;
    }

    public void setDatetimeStart(Date datetimeStart) {
        this.datetimeStart = datetimeStart;
    }

    public Date getDatetimeEnd() {
        return datetimeEnd;
    }

    public void setDatetimeEnd(Date datetimeEnd) {
        this.datetimeEnd = datetimeEnd;
    }

    public int getLeftSideGoal() {
        return leftSideGoal;
    }

    public void setLeftSideGoal(int leftSideGoal) {
        this.leftSideGoal = leftSideGoal;
    }

    public int getRightSideGoal() {
        return rightSideGoal;
    }

    public void setRightSideGoal(int rightSideGoal) {
        this.rightSideGoal = rightSideGoal;
    }

    public int getIdGame() {
        return idGame;
    }

    public void setIdGame(int idGame) {
        this.idGame = idGame;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public List<Coefficient> getCoefficients() {
        return coefficients;
    }

    public void setCoefficients(List<Coefficient> coefficients) {
        this.coefficients = coefficients;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }
}
