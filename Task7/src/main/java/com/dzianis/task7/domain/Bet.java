package com.dzianis.task7.domain;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Date;


/**
 * Created by Denis on 17.06.2017.
 */
public class Bet extends Entity {
    private int id;
    private int money;
    private Date datetime;
    private int leftSideGoal;
    private int rightSideGoal;
    private int idBetType;
    private int idCompetition;
    private int idUser;
    private boolean win;
    private boolean processed;
    private Competition competition;
    private Coefficient coefficient;
    private static final Logger logger = LogManager.getLogger(Bet.class);

    public Competition getCompetition() {

        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public int getLeftSideGoal() {
        return leftSideGoal;
    }

    public void setLeftSideGoal(int leftSideGoal) {
        this.leftSideGoal = leftSideGoal;
    }

    public int getRightSideGoal() {
        return rightSideGoal;
    }

    public void setRightSideGoal(int rightSideGoal) {
        this.rightSideGoal = rightSideGoal;
    }

    public int getIdBetType() {
        return idBetType;
    }

    public void setIdBetType(int idBetType) {
        this.idBetType = idBetType;
    }

    public int getIdCompetition() {
        return idCompetition;
    }

    public void setIdCompetition(int idCompetition) {
        this.idCompetition = idCompetition;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public Coefficient getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(Coefficient coefficient) {
        this.coefficient = coefficient;
    }
}
