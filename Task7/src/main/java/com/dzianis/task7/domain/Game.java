package com.dzianis.task7.domain;

/**
 * Created by Denis on 14.07.2017.
 */
public class Game extends Entity {
    private int id;
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
