package com.dzianis.task7.domain;

/**
 * Created by Denis on 17.06.2017.
 */
public enum RoleType {
     SIGNED_USER, ADMIN, BOOKMAKER, GUEST
}

