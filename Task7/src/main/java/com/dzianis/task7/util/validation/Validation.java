package com.dzianis.task7.util.validation;



import com.dzianis.task7.domain.Entity;

import java.util.List;

public interface Validation<T extends Entity> {
    List<String> getValidationExceptions();

    boolean isValid(T entity);
}
