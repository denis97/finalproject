package com.dzianis.task7.util.dbconnection.query;

/**
 * Created by Denis on 25.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>SQL Queries for work with Bets in database.</b>
 */
public class SQLBetQuery {
    public static final String SQL_SELECT_BET_BY_USER_ID = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`Bets`\n" +
            "WHERE\n" +
            "    `Bets`.`Users_idUsers` = ? ORDER BY `datetime` DESC";
    public static final String SQL_SELECT_BET_BY_COMPETITION_ID = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`Bets`\n" +
            "WHERE\n" +
            "    `Bets`.`Competitions_idCompetitions` = ?";
    public static final String SQL_SELECT_BET_BY_USR_AND_COM = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`Bets`\n" +
            "WHERE\n" +
            "    `Bets`.`Users_idUsers` = ? AND `Bets`.`Competitions_idCompetitions` = ?";
    public static final String SQL_SELECT_BET_BY_ID = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`Bets`\n" +
            "WHERE\n" +
            "    `Bets`.`idBets` = ?";


    public static final String SQL_INSERT_BET = "INSERT INTO\n" +
            "    `bettingsdb`.`Bets`(`money`, `datetime`, `leftSideGoal`, `rightSideGoal`, `BetTypes_idBetTypes`, `Competitions_idCompetitions`, `Users_idUsers`)\n" +
            "     VALUES ( ?, ?, ?, ?, ?, ?, ?)";


    public static final String SQL_DELETE_BET_BY_ID = "DELETE FROM `bettingsdb`.`Bets` \n" +
            "WHERE\n" +
            "    `idBets` = ?";

    public static final String SQL_UPDATE_BET = "UPDATE `bettingsdb`.`Bets` \n" +
            "SET \n" +
            "isWin = ?, " +
            "isProcessed = ?\n" +
            "WHERE\n" +
            "    `idBets` = ?";
}
