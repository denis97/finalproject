package com.dzianis.task7.util.filter;

import com.dzianis.task7.domain.RoleType;
import com.dzianis.task7.domain.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by Denis on 22.07.2017.
 */

@WebFilter(urlPatterns = {"/"}, servletNames = {"Controller"})
public class SetLanguageFilter implements Filter {


    private static final Logger logger = LogManager.getLogger(SetLanguageFilter.class);

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        if (httpServletRequest.getSession().getAttribute("locale")==null) {
            String lang = "en_US";
            if (httpServletRequest.getLocale().getLanguage().equals(new Locale("ru").getLanguage()))
                lang = "ru_RU";
            httpServletRequest.getSession().setAttribute("locale", lang);
        }

        filterChain.doFilter(request, response);
    }

    public void destroy() {
    }

}