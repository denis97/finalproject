package com.dzianis.task7.util.dbconnection.query;

/**
 * Created by Denis on 29.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>SQL Queries for work with Bet Types in database.</b>
 */
public class SQLBetTypeQuery {
    public static final String SQL_SELECT_ALL_BETTYPES = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`BetTypes`";
}
