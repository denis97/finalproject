package com.dzianis.task7.util.paging;

import com.dzianis.task7.controller.SessionRequest;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Denis on 21.07.2017.
 * This class divide content on pages
 */
public class PageListResult<T> {
    private static final String RESULT_CONTENT_PARAM = "list";
    private static final String CURRENT_PAGE_PARAM = "curpage";
    private static final String PAGE_NUMBERS_PARAM = "pages";
    public void setupPages(List<T> elements, int page, int elementsOnPage, SessionRequest requestContent) {

        int pagesCount = elements.size() / elementsOnPage;
        if (elements.size() % elementsOnPage != 0)
            pagesCount++;
        List<T> result = elements.subList(elementsOnPage * (page - 1), elementsOnPage * page < elements.size() ? elementsOnPage * page : elements.size());
        requestContent.setAttribute(RESULT_CONTENT_PARAM, result);
        requestContent.setAttribute(CURRENT_PAGE_PARAM, page);

        List<Integer> range = IntStream.rangeClosed(1, pagesCount)
                .boxed().collect(Collectors.toList());
        requestContent.setAttribute(PAGE_NUMBERS_PARAM, range);
    }
}
