package com.dzianis.task7.util.resource;

import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Locale;
import java.util.ResourceBundle;

public class LocalizedMessageManager {
    private final static Logger logger = LogManager.getLogger(LocalizedMessageManager.class);

    private LocalizedMessageManager() {
    }

    public static String getProperty(String key, SessionRequest requestContent) {
        Locale locale = null;
        try {
            String langCode = (String) requestContent.getSessionAttribute("locale");
            String[] lang = langCode.split("_");
            locale = new Locale.Builder().setLanguage(lang[0]).setRegion(lang[1]).build();
        } catch (NoSuchRequestParameterException e) {
            locale = requestContent.getRequest().getLocale();
        }
        ResourceBundle resourceBundle = ResourceBundle.getBundle("localization.locale", locale);
        return resourceBundle.getString(key);

    }
}
