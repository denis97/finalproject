package com.dzianis.task7.util.dbconnection.query;

/**
 * Created by Denis on 25.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>SQL Queries for work with Coefficient in database.</b>
 */
public class SQLCoefficientQuery {
    public static final String SQL_SELECT_COEFFICIENT_FOR_COMPETITION = "SELECT Competitions_idCompetitions, BetTypes_idBetTypes,coefficient, bettypes.title \n" +
            "FROM bettingsdb.Coefficients " +
            "JOIN bettingsdb.BetTypes ON bettypes.idBetTypes=BetTypes_idBetTypes " +
            "WHERE  Competitions_idCompetitions = ? ORDER BY bettypes.idBetTypes;";
    public static final String SQL_SET_COEFFICIENTS_FOR_COMPETITION = "INSERT INTO `bettingsdb`.`Coefficients` (`Competitions_idCompetitions`, `BetTypes_idBetTypes`, `coefficient`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE coefficient=?";

    public static final String SQL_SELECT_COEFFICIENT_BY_ID = "SELECT Competitions_idCompetitions, BetTypes_idBetTypes,coefficient, bettypes.title \n" +
            "FROM bettingsdb.Coefficients " +
            "JOIN bettingsdb.BetTypes ON bettypes.idBetTypes=BetTypes_idBetTypes " +
            "WHERE  Competitions_idCompetitions = ? AND BetTypes_idBetTypes = ?;";
}
