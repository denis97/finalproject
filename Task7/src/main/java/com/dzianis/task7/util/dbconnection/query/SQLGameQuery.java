package com.dzianis.task7.util.dbconnection.query;

/**
 * Created by Denis on 25.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>SQL Queries for work with Games in database.</b>
 */
public class SQLGameQuery {
    public static final String SQL_SELECT_ALL_GAMES = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`Games`";
    public static final String SQL_SELECT_GAME_BY_ID = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`Games`" +
            "WHERE" +
            "`idGames` = ?";

}
