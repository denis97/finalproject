package com.dzianis.task7.util.validation;



import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.UserService;

import java.util.ArrayList;
import java.util.List;

public class UserValidation implements Validation<User> {
    private static final String LOGIN_PATTERN = "^([a-zA-Z]+)[a-zA-Z\\d_]{4,}$";
    private static final String PASSWORD_PATTERN = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z_.]{6,}$";
    private static final String EMAIL_PATTERN = "^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$";

    private List<String> validationExceptions = new ArrayList<>();


    public List<String> getValidationExceptions() {
        return validationExceptions;
    }

    public boolean isValid(User user) {
        return isLoginValid(user.getLogin()) && isPasswordValid(user.getPassword()) && isEmailValid(user.getEmail());
    }

    private boolean isLoginValid(String login) {
        if (login.matches(LOGIN_PATTERN) && login.length() < 45) {
            try {
                UserService userService = new UserService();
                User usr = userService.findByLogin(login);
                if (usr.getLogin()!=null){
                    validationExceptions.add("This login already registered.");
                    return false;
                }
            } catch (ServiceException|NullPointerException e) {
                return true;
            } catch (InterruptedException e) {
                validationExceptions.add("Something went wrong with username.");
                return false;
            }

        } else {
            validationExceptions.add("User login is not valid.");
            return false;
        }
        return false;
    }

    private boolean isPasswordValid(String password) {
        if (password.matches(PASSWORD_PATTERN) && password.length() < 100) {
            return true;
        } else {
            validationExceptions.add("User password is not valid.");
            return false;
        }
    }

    private boolean isEmailValid(String email) {
        if (email.matches(EMAIL_PATTERN) && email.length() < 100) {
            return true;
        } else {
            validationExceptions.add("User email is not valid.");
            return false;
        }
    }
}
