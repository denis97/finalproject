package com.dzianis.task7.util.filter;

import com.dzianis.task7.domain.RoleType;
import com.dzianis.task7.domain.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Denis on 22.07.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>Filter for control access to the commands.</b>
 */

@WebFilter(urlPatterns = {"/"}, servletNames = {"Controller"})
public class RoleAccessFilter implements Filter {
    private static final String USER_ATTR = "user";
    private static final Logger logger = LogManager.getLogger(RoleAccessFilter.class);

    private enum AdminCommands {
        PROCESS_COMPETITION,
        CREATE_COMPETITION,
        EDIT_COMPETITION,
        FIND_USER,
        EDIT_USER_ADMIN
    }

    ;

    private enum BookmakerCommands {
        SET_COEFFICIENTS
    }

    private enum UserCommands {
        EDIT_USER,
        MAKE_BET,
        PROFILE
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        User user = (User) httpServletRequest.getSession().getAttribute(USER_ATTR);
        if (user == null || user.getRole() == null) {
            user = new User();
            user.setRole(RoleType.GUEST);
            httpServletRequest.getSession().setAttribute(USER_ATTR, user);
        }
        String userCommand = request.getParameter("command");
        if (userCommand == null || userCommand.isEmpty()) {
            userCommand = "HOME";
        }


        if (user.getRole() != RoleType.ADMIN && contains(AdminCommands.class, userCommand.toUpperCase())) {
            logger.info("Illegal request to admin command!");
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/jsp/error/403.jsp");
            return;
        }
        if (user.getRole() != RoleType.ADMIN && user.getRole() != RoleType.BOOKMAKER && contains(BookmakerCommands.class, userCommand.toUpperCase())) {
            logger.info("Illegal request to bookmaker command!");
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/jsp/error/403.jsp");
            return;
        }
        if ((user.getRole() == RoleType.GUEST) && contains(UserCommands.class, userCommand.toUpperCase())) {
            logger.info("Illegal request to user command!");
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/jsp/error/403.jsp");
            return;
        }
        if ((!user.isActive()) && (contains(UserCommands.class, userCommand.toUpperCase()) || contains(UserCommands.class, userCommand.toUpperCase()) || contains(BookmakerCommands.class, userCommand.toUpperCase()))) {
            logger.info("Banned user Illegal request!");
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/jsp/error/banned.jsp");
            return;
        }
        filterChain.doFilter(request, response);
    }

    public void destroy() {
    }

    /**
     *
     * @param clazz Class of the enum
     * @param val Value to find
     * @return True if enum contains value and False if not
     */
    private boolean contains(Class<? extends Enum> clazz, String val) {
        Object[] arr = clazz.getEnumConstants();
        for (Object e : arr) {
            if (((Enum) e).name().equals(val)) {
                return true;
            }
        }
        return false;
    }
}