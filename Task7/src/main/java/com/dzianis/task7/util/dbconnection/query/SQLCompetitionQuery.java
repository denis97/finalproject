package com.dzianis.task7.util.dbconnection.query;

/**
 * Created by Denis on 23.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>SQL Queries for work with Competitions in database.</b>
 */
public class SQLCompetitionQuery {
    public static final String SQL_SELECT_COMPETITION_BY_GAME_ID = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`Competitions`\n" +
            "WHERE\n" +
            "    `Competitions`.`Games_idGames` = ? AND `Competitions`.`datetime_start` > now() ORDER BY datetime_start LIMIT ?,?";
    public static final String SQL_COUNT_COMPETITIONS_BY_GAME_ID = "select count(*) from bettingsdb.Competitions where `Competitions`.`Games_idGames` = ? AND `Competitions`.`datetime_start` > now()";

    public static final String SQL_COUNT_COMPETITIONS_BY_GAME_ID_ADMIN = "select count(*) from bettingsdb.Competitions where `Competitions`.`Games_idGames` = ?";
    public static final String SQL_SELECT_COMPETITION_BY_GAME_ID_ADMIN = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`Competitions`\n" +
            "WHERE\n" +
            "    `Competitions`.`Games_idGames` = ? ORDER BY datetime_start DESC LIMIT ?,?";

    public static final String SQL_SELECT_COMPETITION_UPCOMING = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`Competitions`\n" +
            "WHERE\n" +
            "    `Competitions`.`datetime_start` > now() \n" +
            "ORDER BY\n" +
            "    `Competitions`.`datetime_start`" +
            "LIMIT\n ?";
    public static final String SQL_SELECT_NEXT_COMPETITION = "SELECT  *\n" +
            "  FROM bettingsdb.competitions \n" +
            "  WHERE datetime_start>now() \n" +
            "  ORDER BY datetime_start \n" +
            "  LIMIT 1";
    public static final String SQL_SELECT_LIVE_COMPETITIONS = "SELECT  *\n" +
            "  FROM bettingsdb.competitions " +
            "JOIN bettingsdb.games on games.idGames = competitions.Games_idGames\n" +
            "  WHERE now() " +
            "between datetime_start AND datetime_end;";
    public static final String SQL_SELECT_TOP_COMPETITIONS = "SELECT  * ,\n" +
            "(SELECT count(bets.idBets) FROM bettingsdb.bets WHERE bets.Competitions_idCompetitions = competitions.idCompetitions) AS betsCount\n" +
            "  FROM bettingsdb.competitions \n" +
            "  WHERE datetime_start>now() \n" +
            "  ORDER BY betsCount desc\n" +
            "  limit ?;";
    public static final String SQL_SELECT_COMPETITION_BY_ID = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`Competitions`\n" +
            "WHERE\n" +
            "    `Competitions`.`idCompetitions` = ?";


    public static final String SQL_INSERT_COMPETITION = "INSERT INTO\n" +
            "    `bettingsdb`.`Competitions`(`leftSide`, `rightSide`, `datetime_start`, `datetime_end`, `leftSideGoal`, `rightSideGoal`, `Games_idGames`, `Users_idUsers`)\n" +
            "     VALUES ( ?, ?, ?, ?,?,?,?,?)";

    public static final String SQL_DELETE_COMPETITION_BY_ID = "DELETE FROM `bettingsdb`.`Competitions` \n" +
            "WHERE\n" +
            "    `idCompetitions` = ?";

    public static final String SQL_UPDATE_COMPETITION = "UPDATE `bettingsdb`.`Competitions` \n" +
            "SET \n" +
            "    `leftSide` = ?,\n" +
            "    `rightSide` = ?,\n" +
            "    `datetime_start` = ?,\n" +
            "    `datetime_end` = ?,\n" +
            "    `leftSideGoal` = ?,\n" +
            "    `rightSideGoal` = ?,\n" +
            "    `Games_idGames` = ?,\n" +
            "    `Users_idUsers` = ?,\n" +
            "    `isProcessed` = ?\n" +
            "WHERE\n" +
            "    `idCompetitions` = ?";

}
