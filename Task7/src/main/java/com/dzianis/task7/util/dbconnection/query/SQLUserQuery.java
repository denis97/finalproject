package com.dzianis.task7.util.dbconnection.query;

/**
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>SQL Queries for work with Users in database.</b>
 */
public class SQLUserQuery {
    public static final String SQL_SELECT_USER_BY_ID = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`users`\n" +
            "WHERE\n" +
            "    `users`.`idUsers` = ?\n";

    public static final String SQL_SEARCH_USER = "SELECT * \n" +
            "FROM bettingsdb.users \n" +
            "WHERE username LIKE ? \n" +
            "OR first_name LIKE ? \n" +
            "OR lastname LIKE ? \n" +
            "ORDER BY username";
    public static final String SQL_ALL_USER = "SELECT * \n" +
            "FROM bettingsdb.users";

    public static final String SQL_INSERT_USER = "INSERT INTO\n" +
            "    `bettingsdb`.`users`(`username`, `passwordHash`, `email`, `first_name`, `lastname`, `isActive`, `money_count`, `User_groups_idUser_groups`)\n" +
            "     VALUES (?, ?, ?, ?, ?, True, 0, 1)";

    public static final String SQL_SELECT_USER_BY_LOGIN_AND_PASSWORD = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`users`\n" +
            "WHERE\n" +
            "    `users`.`username` = ?\n" +
            "        AND `users`.`passwordHash` = ?\n";
    public static final String SQL_SELECT_USER_BY_LOGIN = "SELECT * \n" +
            "FROM\n" +
            "    `bettingsdb`.`users`\n" +
            "WHERE\n" +
            "    `users`.`username` = ?\n";

    public static final String SQL_UPDATE_USER_MONEY = "UPDATE `bettingsdb`.`users` \n" +
            "SET \n" +
            "    `money_count` = ?\n" +
            "WHERE\n" +
            "    `idUsers` = ?";

    public static final String SQL_DELETE_USER_BY_ID = "DELETE FROM `bettingsdb`.`users` \n" +
            "WHERE\n" +
            "    `idUsers` = ?";

    public static final String SQL_UPDATE_USER = "UPDATE `bettingsdb`.`users` \n" +
            "SET \n" +
            "    `users`.`email` = ?,\n" +
            "    `users`.`first_name` = ?,\n" +
            "    `users`.`lastname` = ?,\n" +
            "    `users`.`money_count` = ?,\n" +
            "    `users`.`avatar` = ?,\n" +
            "    `users`.`passwordHash` = ?,\n" +
            "    `users`.`isActive` = ?,\n" +
            "    `users`.`User_groups_idUser_groups` = ?\n" +
            "WHERE\n" +
            "    `idUsers` = ?";


}
