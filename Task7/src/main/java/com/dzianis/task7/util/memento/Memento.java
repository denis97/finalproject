package com.dzianis.task7.util.memento;



public class Memento {
    private static Memento instance = null;
    private String currentPage = "/?command=home";

    private Memento() {
    }

    public static Memento getInstance() {
        if (instance == null) {
            instance = new Memento();
        }
        return instance;
    }

    public String getPreviousPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }
}