package com.dzianis.task7.util.tag.custom;

/**
 * Created by Denis on 24.07.2017.
 */
import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class MoneyTag extends TagSupport{
    private static final long serialVersionUID = 1L;

    private int value;


    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            BigDecimal bigDecimal = new BigDecimal(this.value).divide(new BigDecimal(100),2,BigDecimal.ROUND_DOWN);
            pageContext.getOut().print( bigDecimal.toString() );
        } catch(IOException ioException) {
            throw new JspException("Error: " + ioException.getMessage());
        }
        return SKIP_BODY;
    }
}