package com.dzianis.task7.util.tag.custom;

/**
 * Created by Denis on 24.07.2017.
 */

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.math.BigDecimal;

public class CurrentURLTag extends TagSupport{
    private static final long serialVersionUID = 1L;

    private String value;


    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            String result = "/?"+value;
            if (value.indexOf("&page")>-1)
                result = "/?"+value.substring(0,value.indexOf("&page"));
            pageContext.getOut().print(result);
        } catch(IOException ioException) {
            throw new JspException("Error: " + ioException.getMessage());
        }
        return SKIP_BODY;
    }
}