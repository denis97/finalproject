package com.dzianis.task7.command.factory;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.command.CommandEnum;
import com.dzianis.task7.command.factory.action.EmptyCommand;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Denis on 17.06.2017.
 */
public class ActionFactory {
    private static final String COMMAND_PARAM = "command";
    private static final Logger logger = LogManager.getLogger(ActionFactory.class);

    public ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand current = new EmptyCommand();
        String userCommand = request.getParameter(COMMAND_PARAM);

        if (userCommand == null || userCommand.isEmpty()) {
            userCommand="HOME";
        }

        CommandEnum currentEnum = CommandEnum.valueOf(userCommand.toUpperCase());
        current = currentEnum.getCurrentCommand();

        return current;
    }
}

