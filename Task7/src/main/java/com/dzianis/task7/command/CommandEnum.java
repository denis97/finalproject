package com.dzianis.task7.command;

import com.dzianis.task7.command.factory.action.*;
import com.dzianis.task7.command.factory.action.page.FindUserCommand;
import com.dzianis.task7.command.factory.action.page.GameCommand;
import com.dzianis.task7.command.factory.action.page.HomeCommand;
import com.dzianis.task7.command.factory.action.page.ProfileCommand;
import com.dzianis.task7.command.factory.action.user.*;

/**
 * Created by Denis on 11.06.2017.
 */
public enum CommandEnum {

    HOME {
        {
            this.command = new HomeCommand();
        }
    },
    GAME {
        {
            this.command = new GameCommand();
        }
    },

    PROFILE {
        {
            this.command = new ProfileCommand();
        }
    },
    GET_LIVE {
        {
            this.command = new GetLiveCommand();
        }
    },
    CHANGE_LANGUAGE {
        {
            this.command = new ChangeLanguageCommand();
        }
    },
    FIND_USER {
        {
            this.command = new FindUserCommand();
        }
    },
    MAKE_BET {
        {
            this.command = new MakeBetCommand();
        }
    },
    PROCESS_COMPETITION {
        {
            this.command = new ProcessCompetitionCommand();
        }
    },
    CREATE_COMPETITION {
        {
            this.command = new CreateCompetitionCommand();
        }
    },
    EDIT_COMPETITION {
        {
            this.command = new EditCompetitionCommand();
        }
    },
    SET_COEFFICIENTS {
        {
            this.command = new SetCoefficientsCommand();
        }
    },
    EDIT_USER {
        {
            this.command = new EditUserCommand();
        }
    },
    EDIT_USER_ADMIN {
        {
            this.command = new EditUserAdminCommand();
        }
    },

    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },

    SIGN_UP {
        {
           this.command = new SignUpCommand();
        }
    },

    LOGOUT {
        {
           this.command = new LogoutCommand();
        }
    };



    ActionCommand command;

    public ActionCommand getCurrentCommand() {
        return command;
    }
}
