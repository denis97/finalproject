package com.dzianis.task7.command.factory.action.user;


import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.util.memento.Memento;
import com.dzianis.task7.util.resource.ConfigurationManager;

public class LogoutCommand implements ActionCommand {
    private static final String USER_ATTR = "user";
    private static final String INDEX_PAGE_PATH = "path.page.index";

    @Override
    public String execute(SessionRequest requestContent) {
        requestContent.removeSessionAttribute(USER_ATTR);
        String page = INDEX_PAGE_PATH;
        requestContent.setAttribute("go_to", "home");
        return page;
    }
}
