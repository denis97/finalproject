package com.dzianis.task7.command.factory.action;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.domain.Coefficient;
import com.dzianis.task7.domain.Competition;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.CoefficientService;
import com.dzianis.task7.service.CompetitionService;
import com.dzianis.task7.service.UserService;
import com.dzianis.task7.util.resource.ConfigurationManager;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Denis on 14.07.2017.
 *
 * @author Dzianis Hrydziushka
 * @version 1.0
 *          <b>This command allows admins set coefficients for competition.</b>
 *          <br/>This command for ajax requests.
 */
public class SetCoefficientsCommand implements ActionCommand {

    private static final String AJAX_PAGE_PATH = "path.page.ajax";
    private static final String AJAX_RESULT_ATTR = "jsonresult";

    private static final String ID_COMPETITION_ATTR = "idCompetition";
    private static final String COEFFICIENT1_ATTR = "coeff_1";
    private static final String COEFFICIENT2_ATTR = "coeff_2";
    private static final String COEFFICIENTX_ATTR = "coeff_X";


    private final static Logger logger = LogManager.getLogger(SetCoefficientsCommand.class);


    @Override
    public String execute(SessionRequest requestContent) {
        StringBuilder json = new StringBuilder();
        try {
            CoefficientService competitionService = new CoefficientService();
            int idCom = Integer.parseInt(requestContent.getParameter(ID_COMPETITION_ATTR));
            if ((requestContent.getParameter(COEFFICIENT1_ATTR) != "") && (requestContent.getParameter(COEFFICIENT2_ATTR) != "")) {
                BigDecimal coeff1 = new BigDecimal(requestContent.getParameter(COEFFICIENT1_ATTR));
                BigDecimal coeff2 = new BigDecimal(requestContent.getParameter(COEFFICIENT2_ATTR));
                competitionService.setCoefficientForCompetition(idCom, 1, coeff1);
                competitionService.setCoefficientForCompetition(idCom, 2, coeff2);
                json.append("ok");
            }
            if (requestContent.getParameter(COEFFICIENTX_ATTR) != "") {
                BigDecimal coeffX = new BigDecimal(requestContent.getParameter(COEFFICIENTX_ATTR));
                competitionService.setCoefficientForCompetition(idCom, 3, coeffX);
                json.append("ok");
            }
            if (json.length() == 0)
                json.append("Error. You must enter coefficients for both sides!");

        } catch (NoSuchRequestParameterException | ServiceException | InterruptedException e) {
            logger.log(Level.ERROR, e, e);
            json.append("Error! ");
            json.append(e.getMessage());
        }
        requestContent.setAttribute(AJAX_RESULT_ATTR, json);
        return ConfigurationManager.getProperty(AJAX_PAGE_PATH);
    }

}
