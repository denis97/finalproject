package com.dzianis.task7.command.factory.action.user;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.domain.RoleType;
import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.NoSuchPageException;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.UserService;
import com.dzianis.task7.util.resource.ConfigurationManager;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


/**
 * Created by Denis on 14.07.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>This command allows admins to edit all users of the web application.</b>
 * <br/>This command for ajax requests.
 */
public class EditUserAdminCommand implements ActionCommand {

    private static final String AJAX_PAGE_PATH = "path.page.ajax";
    private static final String AJAX_RESULT_ATTR = "jsonresult";

    private static final String FIRSTNAME_PARAM = "firstName";
    private static final String LASTNAME_PARAM = "lastName";
    private static final String EMAIL_PARAM = "email";
    private static final String MONEY_PARAM = "money";
    private static final String BANNED_PARAM = "banned";
    private static final String PASSWORD_PARAM = "password";
    private static final String ROLE_PARAM = "role";
    private static final String ID_PARAM = "idUser";


    private final static Logger logger = LogManager.getLogger(EditUserAdminCommand.class);

    private UserService userService = new UserService();


    @Override
    public String execute(SessionRequest requestContent) {

        StringBuilder json = new StringBuilder();
        Boolean active = false;
        try {
            if (requestContent.getParameter(BANNED_PARAM).equals("on"))
                active= true;
        }catch (NoSuchRequestParameterException e){
            logger.info("Checkbox unchecked and user will be banned");
        }
        try {
            User user = userService.findById(Integer.parseInt(requestContent.getParameter(ID_PARAM)));
            user.setFirstName(requestContent.getParameter(FIRSTNAME_PARAM));
            user.setLastName(requestContent.getParameter(LASTNAME_PARAM));
            user.setEmail(requestContent.getParameter(EMAIL_PARAM));
            user.setMoney(Integer.parseInt(requestContent.getParameter(MONEY_PARAM)));
            user.setRole(RoleType.valueOf(requestContent.getParameter(ROLE_PARAM)));
            user.setActive(active);
            if (requestContent.getParameter(PASSWORD_PARAM)!=""){
                user.setPassword(DigestUtils.sha256Hex(requestContent.getParameter(PASSWORD_PARAM)));
            }
            //user = setAvatar(requestContent, user);
            user = userService.update(user);
            json.append("ok");
        } catch (NoSuchPageException|NoSuchRequestParameterException | ServiceException | InterruptedException e) {
            logger.log(Level.ERROR, e, e);
            json.append("Error! ");
            json.append(e.getMessage());
        }

        requestContent.setAttribute(AJAX_RESULT_ATTR, json);
        return ConfigurationManager.getProperty(AJAX_PAGE_PATH);
    }

}
