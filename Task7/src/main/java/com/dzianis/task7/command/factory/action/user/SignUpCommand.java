package com.dzianis.task7.command.factory.action.user;


import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.NoSuchRequestParameterException;

import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.UserService;
import com.dzianis.task7.util.resource.ConfigurationManager;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.xml.bind.ValidationException;

/**
 * Created by Denis on 17.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>This command for user registration.</b>
 * <br/>This command for ajax requests.
 */

public class SignUpCommand implements ActionCommand {

    private static final String AJAX_PAGE_PATH = "path.page.ajax";
    private static final String AJAX_RESULT_ATTR = "jsonresult";

    private static final String USER_ATTR = "user";
    private static final String LOGIN_PARAM = "login";
    private static final String PASSWORD_PARAM = "password";
    private static final String EMAIL_PARAM = "email";
    private static final String FIRST_NAME_PARAM = "first-name";
    private static final String LAST_NAME_PARAM = "last-name";

    private final static Logger logger = LogManager.getLogger(SignUpCommand.class);


    private UserService userService = new UserService();

    @Override
    public String execute(SessionRequest requestContent) {
        StringBuilder json = new StringBuilder();
       try {
            User user = userService.create(convertToUser(requestContent));
            if (user.getValidationExceptions()!=null)
            for (String e:user.getValidationExceptions()){
                json.append("<br>Error: "+e);
            }
            requestContent.setSessionAttribute(USER_ATTR, user);
            requestContent.setSessionAttribute("userId", user.getId());
           if ((json.length()==0)&&(user.getId()!=0))
               json.append("ok");
           else
               json.append("<br>Please, fix errors!");
       } catch (ServiceException | InterruptedException | NoSuchRequestParameterException | ValidationException e) {
            logger.log(Level.ERROR, e, e);
            json.append("Error! ");
            json.append(e.getMessage());
        }
        requestContent.setAttribute(AJAX_RESULT_ATTR, json);
        return ConfigurationManager.getProperty(AJAX_PAGE_PATH);
    }

    private User convertToUser(SessionRequest requestContent) throws NoSuchRequestParameterException {
        User user = new User();
        user.setLogin(requestContent.getParameter(LOGIN_PARAM));
        user.setPassword(requestContent.getParameter(PASSWORD_PARAM));
        user.setEmail(requestContent.getParameter(EMAIL_PARAM));
        user.setFirstName(requestContent.getParameter(FIRST_NAME_PARAM));
        user.setLastName(requestContent.getParameter(LAST_NAME_PARAM));
        user.setMoney(0);
        return user;
    }
}
