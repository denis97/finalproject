package com.dzianis.task7.command.factory.action;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.domain.Bet;
import com.dzianis.task7.domain.Competition;
import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.NoSuchPageException;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.BetService;
import com.dzianis.task7.service.CompetitionService;
import com.dzianis.task7.service.UserService;
import com.dzianis.task7.util.resource.ConfigurationManager;
import com.google.gson.Gson;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;


/**
 * Created by Denis on 20.07.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>This command returns live competitions.</b>
 * <br/>This command for ajax requests.
 */
public class GetLiveCommand implements ActionCommand {

    private static final String AJAX_PAGE_PATH = "path.page.ajax";
    private static final String AJAX_RESULT_ATTR = "jsonresult";

    private final static Logger logger = LogManager.getLogger(GetLiveCommand.class);

    private CompetitionService competitionService = new CompetitionService();

    @Override
    public String execute(SessionRequest requestContent) {

        StringBuilder json = new StringBuilder();
        try {
            List<Competition> competitions = competitionService.getLiveGames();
            Gson gson = new Gson();
            json.append(gson.toJson(competitions));
        } catch (ServiceException | InterruptedException e) {
            logger.log(Level.ERROR, e, e);
            json.append("Error in getting live games");
        }
        requestContent.setAttribute(AJAX_RESULT_ATTR, json);
        return ConfigurationManager.getProperty(AJAX_PAGE_PATH);
    }

}
