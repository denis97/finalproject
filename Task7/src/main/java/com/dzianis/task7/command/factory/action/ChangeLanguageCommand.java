package com.dzianis.task7.command.factory.action;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import com.dzianis.task7.util.memento.Memento;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Created by Denis on 20.07.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>This command allows you to change the language of the web application.</b>
 */

public class ChangeLanguageCommand implements ActionCommand {
    private static final String LANGUAGE_PARAM = "language";
    private static final String LOCALE_ATTR = "locale";

    private static final Logger logger = LogManager.getLogger(ChangeLanguageCommand.class);

    @Override
    public String execute(SessionRequest requestContent) {
        String page = null;
        try {
            String locale = requestContent.getParameter(LANGUAGE_PARAM);
            requestContent.setSessionAttribute(LOCALE_ATTR, locale);
            Memento memento = Memento.getInstance();
            page = memento.getPreviousPage();
        } catch (NoSuchRequestParameterException e) {
            logger.log(Level.ERROR, e);
        }
        return page;
    }
}