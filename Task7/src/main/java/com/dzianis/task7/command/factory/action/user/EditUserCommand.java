package com.dzianis.task7.command.factory.action.user;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.NoSuchPageException;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.UserService;
import com.dzianis.task7.util.resource.ConfigurationManager;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.Part;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Paths;


/**
 * Created by Denis on 14.07.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>This command allows users to change information about them.</b>
 * <br/>This command for ajax requests.
 */
public class EditUserCommand implements ActionCommand {

    private static final String AJAX_PAGE_PATH = "path.page.ajax";
    private static final String AJAX_RESULT_ATTR = "jsonresult";

    private static final String FIRSTNAME_PARAM = "firstName";
    private static final String LASTNAME_PARAM = "lastName";
    private static final String EMAIL_PARAM = "email";

    private final static Logger logger = LogManager.getLogger(EditUserCommand.class);

    private UserService userService = new UserService();


    @Override
    public String execute(SessionRequest requestContent) {

        StringBuilder json = new StringBuilder();
        try {
            User user = convertToUser(requestContent);
            //user = setAvatar(requestContent, user);
            user = userService.update(user);
            json.append("ok");
        } catch (NoSuchRequestParameterException | ServiceException | InterruptedException e) {
            logger.log(Level.ERROR, e, e);
            json.append("Error! ");
            json.append(e.getMessage());
        }
        requestContent.setAttribute(AJAX_RESULT_ATTR, json);
        requestContent.setAttribute("go_to", "profile");
        return ConfigurationManager.getProperty(AJAX_PAGE_PATH);
    }

    private User convertToUser(SessionRequest requestContent) throws NoSuchRequestParameterException {
        try {
            User loginUser = ((User) requestContent.getSessionAttribute("user"));
            User user = userService.findById(loginUser.getId());
            user.setFirstName(requestContent.getParameter(FIRSTNAME_PARAM));
            user.setLastName(requestContent.getParameter(LASTNAME_PARAM));
            user.setEmail(requestContent.getParameter(EMAIL_PARAM));
            return user;
        } catch (ServiceException | InterruptedException | NoSuchPageException e) {
            logger.error("Error edit user: " + e.getMessage());
            return new User();
        }

    }

    private User setAvatar(SessionRequest requestContent, User user) {
        Part filePart = null; // Retrieves <input type="file" name="file">
        try {
            filePart = requestContent.getRequest().getPart("file");
            String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
            System.out.println(fileName);
            InputStream fileContent = filePart.getInputStream();
            BufferedImage image = ImageIO.read(fileContent);

            //String avatarsrc= user.getId()+"."+fileName.substring(fileName.lastIndexOf(".") + 1);
            String avatarsrc = user.getId() + ".png";
            File file = new File("C://eclipse-EE/apache-tomcat-9.0.0.M18/images/avatar/" + avatarsrc);
            file.createNewFile();
            //OutputStream outputStream = new FileOutputStream(file,false);
            ImageIO.write(image, "png", file);
            user.setAvatar(avatarsrc);
        } catch (IOException | ServletException | NullPointerException e) {
            logger.info("Can't set new avatar " + e.getMessage());
        }


        return user;
    }
}
