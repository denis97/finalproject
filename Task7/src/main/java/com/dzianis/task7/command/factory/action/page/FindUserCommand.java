package com.dzianis.task7.command.factory.action.page;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.domain.Game;
import com.dzianis.task7.domain.RoleType;
import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.CompetitionService;
import com.dzianis.task7.service.GameService;
import com.dzianis.task7.service.UserService;
import com.dzianis.task7.util.paging.PageListResult;
import com.dzianis.task7.util.resource.ConfigurationManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Created by Denis on 17.06.2017.
 *
 * @author Dzianis Hrydziushka
 * @version 1.0
 *          <b>This command allows admins to find other users.</b>
 */
public class FindUserCommand implements ActionCommand {

    private static final String COMMAND_ATTR = "command";
    private static final String LIVE_COM_ATTR = "livecompetitions";
    private static final String NEXT_COM_ATTR = "nextcompetition";
    private static final String TOP_COM_ATTR = "topcompetitions";
    private static final String GAMES_ATTR = "games";
    private static final String USER_ATTR = "user";
    private static final String DATETIME_ATTR = "datetime_now";
    private static final String PAGE_PARAM = "page";
    private static final String SEARCH_PARAM = "search";
    private static final String ERROR_PAGE_PATH = "path.page.404";
    private static final Logger logger = LogManager.getLogger(FindUserCommand.class);

    private String page = "/jsp/users.jsp";
    private CompetitionService competitionService = new CompetitionService();
    private UserService userService = new UserService();
    private GameService gameService = new GameService();
    private int pagenum = 1;


    @Override
    public String execute(SessionRequest requestContent) {

        setPagenum(requestContent);
        searchUser(requestContent);
        setLoginUser(requestContent);

        try {

            requestContent.setAttribute(COMMAND_ATTR, "find_user");
            requestContent.setAttribute(LIVE_COM_ATTR, competitionService.getLiveGames());
            requestContent.setAttribute(NEXT_COM_ATTR, competitionService.getNextGame());
            requestContent.setAttribute(TOP_COM_ATTR, competitionService.getTopGames(5));
            requestContent.setAttribute(GAMES_ATTR, gameService.getAllGames());
            requestContent.setAttribute(DATETIME_ATTR, new Date());
        } catch (ServiceException | InterruptedException e) {
            page = ConfigurationManager.getProperty(ERROR_PAGE_PATH);
        }
        return page;
    }

    private void searchUser(SessionRequest requestContent) {
        try {
            String searchReq = requestContent.getParameter(SEARCH_PARAM);
            PageListResult<User> list = new PageListResult<User>();
            list.setupPages(userService.searchUser(searchReq), pagenum, 5, requestContent);
        } catch (ServiceException | InterruptedException | NoSuchRequestParameterException e) {
            try {
                PageListResult<User> list = new PageListResult<User>();
                list.setupPages(userService.getAll(), pagenum, 5, requestContent);
            } catch (ServiceException | InterruptedException fatale) {
                page = ConfigurationManager.getProperty(ERROR_PAGE_PATH);
            }
            logger.info("Can't search req. Show all user");
        }
    }

    private void setPagenum(SessionRequest requestContent) {
        try {
            pagenum = Integer.parseInt(requestContent.getParameter(PAGE_PARAM));
        } catch (NoSuchRequestParameterException e) {
            logger.info("Request game page without page number");
            pagenum = 1;
        }
    }

    private void setLoginUser(SessionRequest requestContent) {
        try {
            User curUser = (User) requestContent.getSessionAttribute(USER_ATTR);
            requestContent.setAttribute("usr", curUser);
        } catch (NoSuchRequestParameterException e) {
            logger.info("guest user");
        }
    }
}
