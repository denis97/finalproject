package com.dzianis.task7.command;

import com.dzianis.task7.controller.SessionRequest;

/**
 * Created by Denis on 17.06.2017.
 */

public interface ActionCommand {
    String execute(SessionRequest requestContent);
}

