package com.dzianis.task7.command.factory.action;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.domain.Bet;
import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.NoSuchPageException;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.BetService;
import com.dzianis.task7.service.CompetitionService;
import com.dzianis.task7.service.UserService;
import com.dzianis.task7.util.resource.ConfigurationManager;
import com.dzianis.task7.util.resource.LocalizedMessageManager;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.Locale;


/**
 * Created by Denis on 12.07.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>This command allows users to make bet.</b>
 * <br/>This command for ajax requests.
 */
public class MakeBetCommand implements ActionCommand {
    private static final String AJAX_PAGE_PATH = "path.page.ajax";
    private static final String AJAX_RESULT_ATTR = "jsonresult";

    private static final String USER_ATTR = "user";
    private static final String ID_COMPETITION_ATTR = "idCompetition";
    private static final String ID_BETTYPE_ATTR = "idBetType";
    private static final String LEFT_GOAL_ATTR = "leftSideGoal";
    private static final String RIGHT_GOAL_ATTR = "rightSideGoal";
    private static final String MONEY_ATTR = "money";

    private final static Logger logger = LogManager.getLogger(MakeBetCommand.class);

    private UserService userService = new UserService();
    private CompetitionService competitionService = new CompetitionService();
    @Override
    public String execute(SessionRequest requestContent) {

        StringBuilder json = new StringBuilder();
        try {

            BetService betService = new BetService();
            Bet bet = convertToBet(requestContent);
            User user = userService.findById(bet.getIdUser());
            if (betService.findByUserAndCompetition(bet.getIdUser(), bet.getIdCompetition()).size() > 0) {
                json.append(LocalizedMessageManager.getProperty("error.bet.already",requestContent));
            } else if (bet.getMoney() < 0) {
                json.append(LocalizedMessageManager.getProperty("error.bet.money",requestContent));
            }
            else if ((user.getMoney() - bet.getMoney()) < 0) {
                json.append(LocalizedMessageManager.getProperty("error.bet.money",requestContent));
            } else if ((new Date()).after(bet.getCompetition().getDatetimeStart())) {
                json.append(LocalizedMessageManager.getProperty("error.bet.competition_started",requestContent));
            }else {
                bet = betService.create(bet);
                user.setMoney(user.getMoney() - bet.getMoney());
                userService.update(user);
                json.append("ok");
            }

        } catch ( NoSuchRequestParameterException|NoSuchPageException|ServiceException|InterruptedException  e) {
            logger.log(Level.ERROR, e, e);
            json.append(LocalizedMessageManager.getProperty("error.bet.create",requestContent));
        }
        requestContent.setAttribute(AJAX_RESULT_ATTR, json);
        return ConfigurationManager.getProperty(AJAX_PAGE_PATH);
    }

    private Bet convertToBet(SessionRequest requestContent) throws NoSuchRequestParameterException {
        Bet bet = new Bet();
        bet.setIdCompetition(Integer.parseInt(requestContent.getParameter(ID_COMPETITION_ATTR)));
        bet.setIdBetType(Integer.parseInt(requestContent.getParameter(ID_BETTYPE_ATTR)));
        User loginUser = ((User) requestContent.getSessionAttribute(USER_ATTR));
        bet.setIdUser(loginUser.getId());
        //bet.setIdUser(Integer.parseInt(requestContent.getParameter("idUser")));
        bet.setLeftSideGoal(Integer.parseInt(requestContent.getParameter(LEFT_GOAL_ATTR)));
        bet.setRightSideGoal(Integer.parseInt(requestContent.getParameter(RIGHT_GOAL_ATTR)));
        String money = requestContent.getParameter(MONEY_ATTR);
        money = money.replace(".","");
        money = money.replace(",","");
        bet.setMoney(Integer.parseInt(money));
        bet.setDatetime(new Date());
        try {
            bet.setCompetition(competitionService.findById(bet.getIdCompetition()));
        } catch (ServiceException |InterruptedException e) {
            logger.error("Can't get competition while creating bet");
        }
        return bet;
    }
}
