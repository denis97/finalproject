package com.dzianis.task7.command.factory.action.page;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.domain.Competition;
import com.dzianis.task7.domain.Game;
import com.dzianis.task7.domain.RoleType;
import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.CompetitionService;
import com.dzianis.task7.service.GameService;
import com.dzianis.task7.util.paging.PageListResult;
import com.dzianis.task7.util.resource.ConfigurationManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Denis on 17.06.2017.
 *
 * @author Dzianis Hrydziushka
 * @version 1.0
 *          <b>This command shows all competitions for selected game.</b>
 */
public class GameCommand implements ActionCommand {

    private static final int ELEMENTS_ON_PAGE = 5;
    private static final String COMMAND_ATTR = "command";
    private static final String LIVE_COM_ATTR = "livecompetitions";
    private static final String NEXT_COM_ATTR = "nextcompetition";
    private static final String TOP_COM_ATTR = "topcompetitions";
    private static final String GAMES_ATTR = "games";
    private static final String CURRENT_GAME_ATTR = "curgame";
    private static final String USER_ATTR = "user";
    private static final String DATETIME_ATTR = "datetime_now";
    private static final String PAGE_PARAM = "page";
    private static final String ERROR_PAGE_PATH = "path.page.404";
    private static final String RESULT_CONTENT_PARAM = "list";
    private static final String CURRENT_PAGE_PARAM = "curpage";
    private static final String PAGE_NUMBERS_PARAM = "pages";
    private static final Logger logger = LogManager.getLogger(GameCommand.class);

    private String page = "/jsp/game.jsp";
    private CompetitionService competitionService = new CompetitionService();
    private GameService gameService = new GameService();
    private int idGame = 1;
    private int pagenum = 1;
    private boolean isAdmin = false;
    private Game game = null;

    @Override
    public String execute(SessionRequest requestContent) {

        setIdGame(requestContent);
        setPagenum(requestContent);
        setLoginUser(requestContent);

        try {

            requestContent.setAttribute(COMMAND_ATTR, "game");

            int count = competitionService.getCountByGame(idGame,isAdmin);
            int pagesCount = count / ELEMENTS_ON_PAGE;
            if (count % ELEMENTS_ON_PAGE != 0)
                pagesCount++;
            requestContent.setAttribute(RESULT_CONTENT_PARAM, competitionService.findByGameId(idGame, isAdmin,ELEMENTS_ON_PAGE * (pagenum - 1), ELEMENTS_ON_PAGE * pagenum < count ? ELEMENTS_ON_PAGE * pagenum : count));
            requestContent.setAttribute(CURRENT_PAGE_PARAM, pagenum);
            List<Integer> range = IntStream.rangeClosed(1, pagesCount)
                    .boxed().collect(Collectors.toList());
            requestContent.setAttribute(PAGE_NUMBERS_PARAM, range);
            requestContent.setAttribute(LIVE_COM_ATTR, competitionService.getLiveGames());
            requestContent.setAttribute(NEXT_COM_ATTR, competitionService.getNextGame());
            requestContent.setAttribute(TOP_COM_ATTR, competitionService.getTopGames(5));
            requestContent.setAttribute(GAMES_ATTR, gameService.getAllGames());
            requestContent.setAttribute(CURRENT_GAME_ATTR, game);
            requestContent.setAttribute(DATETIME_ATTR, new Date());
        } catch (ServiceException | InterruptedException e) {
            page = ConfigurationManager.getProperty(ERROR_PAGE_PATH);
        }
        return page;
    }

    private void setLoginUser(SessionRequest requestContent) {
        try {
            User curUser = (User) requestContent.getSessionAttribute(USER_ATTR);
            requestContent.setAttribute("usr", curUser);
            isAdmin = (curUser.getRole() == RoleType.ADMIN);
        } catch (NoSuchRequestParameterException e) {
            logger.info("guest user");
        }
    }

    private void setPagenum(SessionRequest requestContent) {
        try {
            pagenum = Integer.parseInt(requestContent.getParameter(PAGE_PARAM));
        } catch (NoSuchRequestParameterException e) {
            logger.info("Request game page without page number");
            pagenum=1;
        }
    }

    private void setIdGame(SessionRequest requestContent) {
        try {
            idGame = Integer.parseInt(requestContent.getParameter("idGame"));
            game = gameService.findById(idGame);
        } catch (ServiceException | InterruptedException | NoSuchRequestParameterException e) {
            logger.error("Can't get game id");
            page = ConfigurationManager.getProperty(ERROR_PAGE_PATH);
        }
    }
}
