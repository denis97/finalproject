package com.dzianis.task7.command.factory.action;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.util.resource.ConfigurationManager;

/**
 * Created by Denis on 17.06.2017.
 */
public class EmptyCommand implements ActionCommand {

    private static final String ERROR_PAGE_PATH = "path.page.404";

    @Override
    public String execute(SessionRequest requestContent) {
        return ConfigurationManager.getProperty(ERROR_PAGE_PATH);
    }
}
