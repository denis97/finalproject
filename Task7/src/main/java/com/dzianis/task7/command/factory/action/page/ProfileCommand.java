package com.dzianis.task7.command.factory.action.page;



import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.domain.Bet;
import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.NoSuchPageException;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.BetService;
import com.dzianis.task7.service.GameService;
import com.dzianis.task7.service.UserService;
import com.dzianis.task7.util.paging.PageListResult;
import com.dzianis.task7.util.resource.ConfigurationManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Created by Denis on 09.07.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>This is profile page of the user. Contains all bets of the user.</b>
 */
public class ProfileCommand implements ActionCommand {

    private static final String ERROR_PAGE_PATH = "path.page.404";
    private static final Logger logger = LogManager.getLogger(ProfileCommand.class);


    @Override
    public String execute(SessionRequest requestContent) {
        String page = "/jsp/profile.jsp";
        User user = null;
        try {
            UserService userService = new UserService();
            user = userService.findById(((User) requestContent.getSessionAttribute("user")).getId());
            requestContent.setAttribute("usr", user);
        } catch (NoSuchRequestParameterException | ServiceException|InterruptedException|NoSuchPageException e) {
            page = ConfigurationManager.getProperty(ERROR_PAGE_PATH);

        }
        int pagenum = 1;
        try {
            pagenum = Integer.parseInt(requestContent.getParameter("page"));
        } catch (NoSuchRequestParameterException e) {
            logger.info("Request game page without page number");
        }
        try {
            requestContent.setAttribute("command", "profile");
            BetService betService = new BetService();
            PageListResult<Bet> pageListResult = new PageListResult<Bet>();
            pageListResult.setupPages(betService.findByUser(user.getId()),pagenum,5,requestContent);
            requestContent.setAttribute("datetime_now", new Date());
            GameService gameService = new GameService();
            requestContent.setAttribute("games", gameService.getAllGames());
        } catch (ServiceException|InterruptedException e) {
            page = ConfigurationManager.getProperty(ERROR_PAGE_PATH);
        }
        return page;
    }
}
