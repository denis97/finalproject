package com.dzianis.task7.command.factory.action;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.domain.*;
import com.dzianis.task7.exception.NoSuchPageException;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.BetService;
import com.dzianis.task7.service.CompetitionService;
import com.dzianis.task7.service.UserService;
import com.dzianis.task7.util.resource.ConfigurationManager;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by Denis on 14.07.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>This command allows admins to process results of the competition and сharge money to users.</b>
 * <br/>This command for ajax requests.
 */
public class ProcessCompetitionCommand implements ActionCommand {

    private static final String AJAX_PAGE_PATH = "path.page.ajax";
    private static final String AJAX_RESULT_ATTR = "jsonresult";
    private final static Logger logger = LogManager.getLogger(ProcessCompetitionCommand.class);

    private UserService userService = new UserService();
    private BetService betService = new BetService();
    private CompetitionService competitionService = new CompetitionService();

    @Override
    public String execute(SessionRequest requestContent) {

        StringBuilder json = new StringBuilder();
        try {
            Competition com = competitionService.findById(Integer.parseInt(requestContent.getParameter("idCom")));
            User curUser = userService.findById(((User) requestContent.getSessionAttribute("user")).getId());
            if ((!com.isProcessed()) && (new Date().after(com.getDatetimeEnd()))) {
                List<Bet> bets = betService.findByCompetition(com.getId());
                for (Bet bet : bets) {
                    if (!bet.isProcessed()) {
                        if (isBetWin(com, bet)) {
                            bet.setWin(true);
                            BigDecimal coeff = bet.getCoefficient().getCoefficient();
                            int winMoney = new BigDecimal(bet.getMoney()).multiply(coeff).intValue();
                            User user = userService.findById(bet.getIdUser());
                            user.setMoney(user.getMoney() + winMoney);
                            userService.update(user);
                        } else {
                            bet.setWin(false);
                        }
                        bet.setProcessed(true);
                        betService.update(bet);
                    } else {
                        json.append("Warning! Competition is not processed but betid=" + bet.getId() + " is processed!<br>");
                    }
                }
                com.setProcessed(true);
                competitionService.update(com);
                json.append("ok");
            } else {
                json.append("This competition can't be processed");
            }


        } catch (NoSuchPageException | NoSuchRequestParameterException | ServiceException | InterruptedException e) {
            logger.log(Level.ERROR, e, e);
            json.append("Error! ");
            json.append(e.getMessage());
        }
        requestContent.setAttribute(AJAX_RESULT_ATTR, json);
        return ConfigurationManager.getProperty(AJAX_PAGE_PATH);
    }

    private boolean isBetWin(Competition com, Bet bet) {
        if ((bet.getIdBetType() == 3) && (bet.getLeftSideGoal() == com.getLeftSideGoal()) && (bet.getRightSideGoal() == com.getRightSideGoal())) {
            return true;
        } else if ((bet.getIdBetType() == 1) && (com.getLeftSideGoal() > com.getRightSideGoal())) {
            return true;
        } else if ((bet.getIdBetType() == 2) && (com.getLeftSideGoal() < com.getRightSideGoal())) {
            return true;
        }
        return false;
    }
}
