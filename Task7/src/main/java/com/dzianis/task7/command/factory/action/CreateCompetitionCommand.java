package com.dzianis.task7.command.factory.action;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.domain.Bet;
import com.dzianis.task7.domain.Competition;
import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.NoSuchPageException;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.BetService;
import com.dzianis.task7.service.CompetitionService;
import com.dzianis.task7.service.UserService;
import com.dzianis.task7.util.resource.ConfigurationManager;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Denis on 14.07.2017.
 *
 * @author Dzianis Hrydziushka
 * @version 1.0
 *          <b>This command allows admins to create new competition.</b>
 *          <br/>This command for ajax requests.
 */
public class CreateCompetitionCommand implements ActionCommand {

    private static final String AJAX_PAGE_PATH = "path.page.ajax";
    private static final String AJAX_RESULT_ATTR = "jsonresult";
    private static final String DATETIME_PATERN = "yyyy-MM-dd'T'HH:mm";

    private static final String LEFT_SIDE_PARAM = "leftSide";
    private static final String RIGHT_SIDE_PARAM = "rightSide";
    private static final String LEFT_GOAL_PARAM = "leftSideGoal";
    private static final String RIGHT_GOAL_PARAM = "rightSideGoal";
    private static final String DATETIME_START_PARAM = "datetimeStart";
    private static final String DATETIME_END_PARAM = "datetimeEnd";
    private static final String ID_USER_PARAM = "idUser";
    private static final String ID_GAME_PARAM = "comGame";

    private static final String DATETIME_ERROR_MSG = "Error in datetime!";

    private final static Logger logger = LogManager.getLogger(CreateCompetitionCommand.class);


    @Override
    public String execute(SessionRequest requestContent) {
        StringBuilder json = new StringBuilder();
        try {
            CompetitionService competitionService = new CompetitionService();
            Competition competition = convertToCompetition(requestContent);
            if (competition.getDatetimeEnd().after(competition.getDatetimeStart())) {
                competition = competitionService.create(competition);
                json.append("ok");
            } else {
                json.append(DATETIME_ERROR_MSG);
            }
        } catch (NoSuchRequestParameterException | ServiceException | InterruptedException e) {
            logger.log(Level.ERROR, e, e);
            json.append("Error! ");
            json.append(e.getMessage());
        }
        requestContent.setAttribute(AJAX_RESULT_ATTR, json);
        return ConfigurationManager.getProperty(AJAX_PAGE_PATH);
    }

    private Competition convertToCompetition(SessionRequest requestContent) throws NoSuchRequestParameterException {
        Competition com = new Competition();
        com.setLeftSide(requestContent.getParameter(LEFT_SIDE_PARAM));
        com.setRightSide(requestContent.getParameter(RIGHT_SIDE_PARAM));
        com.setLeftSideGoal(Integer.parseInt(requestContent.getParameter(LEFT_GOAL_PARAM)));
        com.setRightSideGoal(Integer.parseInt(requestContent.getParameter(RIGHT_GOAL_PARAM)));
        SimpleDateFormat format = new SimpleDateFormat(DATETIME_PATERN);
        Date parsed = null;
        Date parsed2 = null;
        try {
            parsed = format.parse(requestContent.getParameter(DATETIME_START_PARAM));
            parsed2 = format.parse(requestContent.getParameter(DATETIME_END_PARAM));
        } catch (ParseException e) {
            logger.error(DATETIME_ERROR_MSG + e.getMessage());
            e.printStackTrace();
        }
        com.setDatetimeStart(parsed);
        com.setDatetimeEnd(parsed2);
        com.setIdUser(Integer.parseInt(requestContent.getParameter(ID_USER_PARAM)));
        com.setIdGame(Integer.parseInt(requestContent.getParameter(ID_GAME_PARAM)));
        return com;
    }
}
