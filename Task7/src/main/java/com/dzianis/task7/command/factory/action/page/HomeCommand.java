package com.dzianis.task7.command.factory.action.page;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.CompetitionService;
import com.dzianis.task7.service.GameService;
import com.dzianis.task7.util.resource.ConfigurationManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;




/**
 * Created by Denis on 17.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>This is home page of the web application.</b>
 */
public class HomeCommand implements ActionCommand {

    private static final String NEXT_COMPETITIONS_ATTR = "nextcompetitions";
    private static final String LIVE_COM_ATTR = "livecompetitions";
    private static final String NEXT_COM_ATTR = "nextcompetition";
    private static final String TOP_COM_ATTR = "topcompetitions";
    private static final String GAMES_ATTR = "games";
    private static final String USER_ATTR = "user";
    private static final String ERROR_PAGE_PATH = "path.page.404";
    private static final Logger logger = LogManager.getLogger(HomeCommand.class);
    private String page = "/jsp/home.jsp";
    private CompetitionService competitionService = new CompetitionService();
    private GameService gameService = new GameService();

    @Override
    public String execute(SessionRequest requestContent) {

        setLoginUser(requestContent);
        try {
            requestContent.setAttribute(NEXT_COMPETITIONS_ATTR, competitionService.getUpcomingGames(5));
            requestContent.setAttribute(LIVE_COM_ATTR, competitionService.getLiveGames());
            requestContent.setAttribute(NEXT_COM_ATTR, competitionService.getNextGame());
            requestContent.setAttribute(TOP_COM_ATTR, competitionService.getTopGames(5));
            requestContent.setAttribute(GAMES_ATTR, gameService.getAllGames());
        } catch (ServiceException|InterruptedException e) {
            page = ConfigurationManager.getProperty(ERROR_PAGE_PATH);
        }
        return page;
    }

    private void setLoginUser(SessionRequest requestContent) {
        try {
            User curUser = (User) requestContent.getSessionAttribute(USER_ATTR);
            requestContent.setAttribute("usr", curUser);
        } catch (NoSuchRequestParameterException e) {
            logger.info("home guest");
        }
    }
}
