package com.dzianis.task7.command.factory.action.user;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.controller.SessionRequest;
import com.dzianis.task7.domain.User;
import com.dzianis.task7.exception.NoSuchRequestParameterException;
import com.dzianis.task7.exception.ServiceException;
import com.dzianis.task7.service.UserService;
import com.dzianis.task7.util.resource.ConfigurationManager;
import com.dzianis.task7.util.resource.LocalizedMessageManager;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;



/**
 * Created by Denis on 17.06.2017.
 * @author Dzianis Hrydziushka
 * @version 1.0
 * <b>This command allows users to login.</b>
 * <br/>This command for ajax requests.
 */


public class LoginCommand implements ActionCommand {

    private static final String AJAX_PAGE_PATH = "path.page.ajax";
    private static final String AJAX_RESULT_ATTR = "jsonresult";

    private static final String LOGIN_PARAM = "known-login";
    private static final String PASSWORD_PARAM = "known-password";
    private static final String USER_ATTR = "user";

    private static final Logger logger = LogManager.getLogger(LoginCommand.class);

    private UserService userService = new UserService();

    @Override
    public String execute(SessionRequest requestContent) {
        StringBuilder json = new StringBuilder();
        try {
            String login = requestContent.getParameter(LOGIN_PARAM);
            String password = requestContent.getParameter(PASSWORD_PARAM);
            User user = userService.findByLoginAndPassword(login, password);
            if (user != null) {
                requestContent.setSessionAttribute(USER_ATTR, user);
                requestContent.setSessionAttribute("userId", user.getId());
                json.append("ok");
            } else {
                json.append(LocalizedMessageManager.getProperty("login.wrong",requestContent));
            }
        } catch (InterruptedException | NoSuchRequestParameterException | ServiceException e) {
            logger.log(Level.ERROR, e, e);
            json.append(LocalizedMessageManager.getProperty("500_text",requestContent));
        }
        requestContent.setAttribute(AJAX_RESULT_ATTR, json);
        return ConfigurationManager.getProperty(AJAX_PAGE_PATH);
    }
}