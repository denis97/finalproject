package com.dzianis.task7.controller;

import com.dzianis.task7.command.ActionCommand;
import com.dzianis.task7.command.factory.ActionFactory;
import com.dzianis.task7.util.dbconnection.ConnectionPool;
import com.dzianis.task7.util.memento.Memento;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Denis on 11.06.2017.
 *
 * @author Dzianis Hrydziushka
 * @version 1.0
 */
@WebServlet("")
@MultipartConfig
public class Controller extends HttpServlet {

    private static final String ENCODING = "UTF-8";
    private static final String PREVIOUS_PAGE = "previous_page";
    private static final String CONTROLLER_PATH = "/?";


    private static final Logger logger = LogManager.getLogger(Controller.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public void destroy() {
        ConnectionPool.getInstance().closePool();
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding(ENCODING);
        response.setCharacterEncoding(ENCODING);
        String page;
        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineCommand(request);

        SessionRequest requestContent = new SessionRequest(request);
        page = command.execute(requestContent);
        requestContent.insertValues(request);


        if (request.getAttribute("go_to") != null) {
            response.sendRedirect("/?command=" + request.getAttribute("go_to"));
            return;
        }

        setPreviousPage(request);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);

        dispatcher.forward(request, response);

    }

    private void setPreviousPage(HttpServletRequest request) {
        if ("GET".equals(request.getMethod())) {
            String currentPage = CONTROLLER_PATH + (request.getQueryString() == null ? "command=home" : request.getQueryString());
            Memento memento = Memento.getInstance();
            String previousPage = memento.getPreviousPage();
            memento.setCurrentPage(currentPage);

            request.setAttribute(PREVIOUS_PAGE, previousPage);
        }


    }
}
