package com.dzianis.task7.exception;

/**
 * Created by Denis on 17.06.2017.
 * This exception is used when you try to get a nonexistent parameter in the request.
 */
public class NoSuchRequestParameterException extends Exception {
    public NoSuchRequestParameterException() {
        super();
    }

    public NoSuchRequestParameterException(String message) {
        super(message);
    }

    public NoSuchRequestParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchRequestParameterException(Throwable cause) {
        super(cause);
    }
}
