package com.dzianis.task7.exception;

/**
 * Created by Denis on 17.06.2017.
 * This exception is used when problems occur at the Service level.
 */
public class ServiceException extends Exception {
    public ServiceException() {
        super();
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
