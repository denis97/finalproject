package com.dzianis.task7.exception;

/**
 * Created by Denis on 17.06.2017.
 * This exception is used when you try to get a nonexistent page.
 */
public class NoSuchPageException extends Exception {
    public NoSuchPageException() {
        super();
    }

    public NoSuchPageException(String message) {
        super(message);
    }

    public NoSuchPageException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchPageException(Throwable cause) {
        super(cause);
    }
}
