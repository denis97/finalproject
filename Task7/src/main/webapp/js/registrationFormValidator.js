// JavaScript Document
var ul = document.getElementById("register_errors");
var lang = "en_US";
function setLang(language) {
    lang=language;
}
function getMsg(key) {
    switch (lang){
        case "en_US":
            return getEnMsg(key);
        case "ru_RU":
            return getRuMsg(key);
    }
}
function getEnMsg(key) {
    switch (key){
        case "username":
            return "You have entered an invalid username!";
        case "password":
            return "Invalid password format!";
        case "confirm-password":
            return "Passwords don't match";
        case "email":
            return "You have entered an invalid email !";
        case "first_name":
            return "You have entered an invalid First Name!";
        case "last_name":
            return "You have entered an invalid Last Name!";
    }
}
function getRuMsg(key) {
    switch (key){
        case "username":
            return "Вы ввели неверное имя пользователя!";
        case "password":
            return "Пароль неверного формата!";
        case "confirm-password":
            return "Пароли не совпадают";
        case "email":
            return "Вы ввели неверный email адрес!";
        case "first_name":
            return "Вы ввели неверное имя!";
        case "last_name":
            return "Вы ввели неверную фамилию!";
    }
}

function addErrorMsg(el_id) {
    if (document.getElementById("error_"+el_id) == null) {
        var li = document.createElement("li");
        li.setAttribute("id", "error_"+el_id);
        li.appendChild(document.createTextNode(getMsg(el_id)));
        ul.appendChild(li);
        var field = document.getElementById("id_"+el_id);
        field.style.backgroundColor="#ffcfcf";
    }
}

function deleteErrorMsg(el_id) {
    if (document.getElementById("error_"+el_id) != null) {
        var el = document.getElementById("error_"+el_id);
        el.parentNode.removeChild(el);
        var field = document.getElementById("id_"+el_id);
        field.style.backgroundColor="#f2f2f2";
    }
}

function validatePassword() {
    var password = document.getElementById("id_password");
    var confirm_password = document.getElementById("id_confirm-password");
    if (password.value != confirm_password.value) {
        addErrorMsg("confirm-password");

    } else {
        deleteErrorMsg("confirm-password");
    }
    var passwordformat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z_.]{6,}$/;
    if (password.value.match(passwordformat)) {
        deleteErrorMsg("password");
    }
    else {
        addErrorMsg("password");
    }
}


function validateEmail() {
    var uemail = document.getElementById("id_email");
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (uemail.value.match(mailformat)) {
        deleteErrorMsg("email");
    }
    else {
        addErrorMsg("email");
    }
}


function validateUserName() {
    var usr = document.getElementById("id_username");
    var usernameformat = /^[a-zA-Z][a-zA-Z_0-9]+$/;
    if (usr.value.match(usernameformat)&&(usr.value.length>=5)) {
    deleteErrorMsg("username");
    }
    else {
        addErrorMsg("username");
    }
}

function validateFirstName() {
    var el = document.getElementById("id_first_name");
    var usernameformat = /^[a-zA-Zа-яА-ЯёЁ]+$/;
    if (el.value.match(usernameformat)) {
        deleteErrorMsg("first_name");
    }
    else {
        addErrorMsg("first_name");
    }
}
function validateLastName() {
    var el = document.getElementById("id_last_name");
    var usernameformat = /^[a-zA-Zа-яА-ЯёЁ]+$/;
    if (el.value.match(usernameformat)) {
        deleteErrorMsg("last_name");
    }
    else {
        addErrorMsg("last_name");
    }
}

