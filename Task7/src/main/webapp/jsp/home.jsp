<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 17.06.2017
  Time: 18:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><fmt:message key="home_page"/> - Sport Bet Site</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="Description" content="Sports betting site.">
    <meta name="Keywords" content="sports betting, sport, ставки на спорт, тотализатор">

    <link rel="shortcut icon" href="images/favicon.png">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<c:import url="part/header.jsp"/>
<main>
    <div class="main__content">
        <c:import url="block/block_hot.jsp"/>
        <h2 class="center-title"><fmt:message key="com.upcoming"/></h2>
        <div id="last-competitions">
            <table class="competitions-table width-full">
                <tbody>

                <tr>
                    <th scope="col"><fmt:message key="game"/></th>
                    <th scope="col"><fmt:message key="com"/></th>
                    <th scope="col"><fmt:message key="time"/></th>
                </tr>
                <c:forEach var="com" items="${nextcompetitions}">
                    <tr>
                        <td><img class="sport-icon" src="images/icons/sports/${com.idGame}-b.svg" alt="sport icon"/>
                            <fmt:message key="${com.game.title}"/></td>
                        <td>
                            <div class="competition"><c:out value="${com.leftSide}"/>-<c:out value="${com.rightSide}"/>
                                <br>
                                <c:forEach var="coef" items="${com.coefficients}">
                                    <a class="coefficient" onclick="
                                            prepeareBetForm(${com.id},${coef.idCoefficientType},${usrid},'${com.leftSide}-${com.rightSide}(${coef.title}:${coef.coefficient})');"><c:out
                                            value="${coef.title}"/>: <c:out
                                            value="${coef.coefficient}"/></a>
                                </c:forEach>
                                <c:set var="admin" value="ADMIN"/>
                                <c:set var="bookmaker" value="BOOKMAKER"/>
                                <c:if test="${usr.role==admin||usr.role==bookmaker}">
                                    <a class="coefficient" onclick="prepeareCoefficientForm(${com.id}<c:forEach var="coef" items="${com.coefficients}">,${coef.coefficient}</c:forEach>);"><fmt:message key="set"/></a>
                                </c:if>
                            </div>
                        </td>
                        <td><fmt:formatDate type="both" dateStyle="long" timeStyle="short"
                                            value="${com.datetimeStart}"/>
                            <c:set var="admin" value="ADMIN"/>
                            <c:if test="${usr.role==admin}">
                                <a class="coefficient"
                                   onclick="prepeareEditComForm(${com.idGame},'${com.leftSide}','${com.rightSide}',${com.leftSideGoal},${com.rightSideGoal}, '<fmt:formatDate pattern="yyyy-MM-dd'T'HH:mm"
                                                       value="${com.datetimeStart}"/>', '<fmt:formatDate
                                           pattern="yyyy-MM-dd'T'HH:mm"
                                           value="${com.datetimeEnd}"/>','${com.id}');"><fmt:message key="edit"/></a>
                            </c:if>
                        </td>
                    </tr>


                </c:forEach>


                </tbody>
            </table>


        </div>
    </div>
    <c:import url="part/menu_left.jsp"/>

    <div class="main__sidebar main__sidebar--second">

        <c:import url="block/block_live.jsp"/>
        <c:import url="block/block_popular.jsp"/>


    </div>
</main>
<c:import url="part/footer.jsp"/>
<c:import url="modal/makeBetModal.jsp"/>
<c:set var="admin" value="ADMIN"/>
<c:set var="bookmaker" value="BOOKMAKER"/>
<c:if test="${usr.role==admin||usr.role==bookmaker}">
    <c:import url="modal/editCoefficientsModal.jsp"/>
    <c:import url="modal/editCompetitionModal.jsp"/>
</c:if>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>

</html>


