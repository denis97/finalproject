<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 09.07.2017
  Time: 19:08
  To change this template use File | Settings | File Templates.
--%>
<fmt:setLocale value="${sessionScope.locale}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="main__sidebar main__sidebar--first">
    <div id="sportsnav">
        <div class="menu-wrapper">
            <c:import url="/jsp/part/mobile_menu.jsp"/>
            <ul class="left-menu">
                <li class="title"><fmt:message key="sports"/></li>
                <c:forEach var="game" items="${games}">
                    <li><a href="/?command=game&idGame=${game.id}"><span class="left-menu-item"><img
                            class="sport-icon-menu" src="images/icons/sports/${game.id}.svg" alt=""/> <fmt:message
                            key="${game.title}"/></span></a>
                    </li>
                </c:forEach>
                <!--<li class="title"> <fmt:message key="sports.other"/></li>-->
            </ul>
        </div>

    </div>
</div>