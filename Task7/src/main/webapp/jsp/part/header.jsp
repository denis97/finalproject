<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 09.07.2017
  Time: 18:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<header>
    <div class="logo"><a href="/?command=home"><img src="images/logo.png" alt="logo" height="80"/></a></div>
    <ul class="actions large-only">
        <li>
            <form action="/" method="get">
                <input type="hidden" name="command" value="change_language">
                <select title="language-select" class="dropdown-languages" name="language"
                        onchange="this.form.submit()">
                    <option selected disabled><fmt:message key="language"/></option>
                    <option value="en_US">English</option>
                    <option value="ru_RU">Русский</option>
                </select>
            </form>
        </li>
        <c:choose>
            <c:when test="${usr.login!=null}">
                <c:set var = "usrid" scope = "session" value = "${usr.id}"/>
                <li><a href="/?command=profile"><fmt:message key="hello"/> ${usr.firstName} ${usr.lastName}</a></li>
                <li><a href="/?command=logout"><fmt:message key="logout"/></a></li>
            </c:when>

            <c:otherwise>
                <c:set var = "usrid" scope = "session" value = "${-1}"/>
                <li><a data-toggle="modal" data-target="#loginModal"><fmt:message key="login.btn"/></a></li>
                <li><a data-toggle="modal" data-target="#registerModal"><fmt:message key="signup.btn"/></a></li>
            </c:otherwise>
        </c:choose>
        <c:set var="admin" value="ADMIN"/>

        <c:if test="${usr.role==admin}">
            <li><a data-toggle="modal" data-target="#makeCompetitionModal"><fmt:message key="com.create"/></a></li>
            <li><a href="/?command=find_user"><fmt:message key="user.find.btn"/></a></li>
        </c:if>


    </ul>


</header>


<c:import url="/jsp/modal/loginModal.jsp"/>
<c:import url="/jsp/modal/registerModal.jsp"/>

<c:if test="${usr.role==admin}">
    <c:import url="/jsp/modal/makeCompetitionModal.jsp"/>
</c:if>