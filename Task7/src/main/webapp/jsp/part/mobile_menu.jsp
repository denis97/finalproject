<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 23.07.2017
  Time: 22:19
  To change this template use File | Settings | File Templates.
--%>
<ul class="left-menu mobile-only">
    <li class="title"><fmt:message key="main_menu"/></li>
    <c:choose>
        <c:when test="${usr.login!=null}">
            <c:set var="usrid" scope="session" value="${usr.id}"/>
            <li><a href="/?command=profile"><span class="left-menu-item"><fmt:message
                    key="hello"/> ${usr.firstName} ${usr.lastName}</span></a></li>
            <li><a href="/?command=logout"><span class="left-menu-item"><fmt:message key="logout"/></span></a></li>
        </c:when>

        <c:otherwise>
            <c:set var="usrid" scope="session" value="${-1}"/>
            <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="left-menu-item"><fmt:message
                    key="login.btn"/></span></a></li>
            <li><a href="#" data-toggle="modal" data-target="#registerModal"><span class="left-menu-item"><fmt:message
                    key="signup.btn"/></span></a></li>
        </c:otherwise>
    </c:choose>
    <c:set var="admin" value="ADMIN"/>

    <c:if test="${usr.role==admin}">
        <li><a href="#" data-toggle="modal" data-target="#makeCompetitionModal"><span class="left-menu-item"><fmt:message
                key="com.create"/></span></a></li>
        <li><a href="/?command=find_user"><span class="left-menu-item"><fmt:message key="user.find.btn"/></span></a></li>
    </c:if>
    <li>
        <form action="/" method="get" class="left-menu-item">
            <input type="hidden" name="command" value="change_language">
            <select title="language-select" class="dropdown-languages" name="language"
                    onchange="this.form.submit()">
                <option selected disabled><fmt:message key="language"/></option>
                <option value="en_US">English</option>
                <option value="ru_RU">Russian</option>
            </select>
        </form>
    </li>
</ul>