<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/myCustomTag" prefix="my" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 21.07.2017
  Time: 20:54
  To change this template use File | Settings | File Templates.
--%>
<div class="text-center">
    <ul class="pagination">
        <c:forEach var="page" items="${pages}">
            <c:if test="${page==curpage}">
                <li class="active"><a>${page}</a></li>
            </c:if>
            <c:if test="${page!=curpage}">
               <li><a href="<my:geturl value="${requestScope['javax.servlet.forward.query_string']}"/>&page=${page}">${page}</a></li>
                <!--<li><a href="/?command=${command}<c:if test="${curgame.id != null}">&idGame=${curgame.id}</c:if>&page=${page}">${page}</a></li>-->
            </c:if>
        </c:forEach>
    </ul>
</div>
