<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 17.06.2017
  Time: 18:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>500 - Sport Bet Site</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="Description" content="Sports betting site.">
    <meta name="Keywords" content="sports betting, sport, ставки на спорт, тотализатор">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/error.css">
    <link rel="shortcut icon" href="/images/favicon.png">

</head>
<body>
<header>
    <div class="logo"><img src="/images/logo.png" alt="logo" height="80"/></div>
</header>
<section class="error-section">
    <div class="error-block">
        <div class="block-header"><h3><fmt:message key="error"/> 500</h3></div>
        <div class="block-content">
            <p><fmt:message key="500_text"/></p>
            <p><a id="id_submit" href="/"><fmt:message key="go_home"/></a></p>
        </div>
    </div>
    </div>
</section>
<c:import url="/jsp/part/footer.jsp"/>
</body>
</html>

