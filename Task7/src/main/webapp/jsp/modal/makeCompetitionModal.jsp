<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 12.07.2017
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<!-- Make Com Modal -->
<div class="modal fade" id="makeCompetitionModal" tabindex="-1" role="dialog"
     aria-labelledby="makeCompetitionModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="makeCompetitionModalLabel"><fmt:message key="com.create_title"/></h4>
            </div>
            <form class="input-line" id="makeCompetitionForm" method="post">

                <div class="modal-body">
                    <input type="hidden" name="command" value="create_competition">
                    <input type="hidden" id="com_idUser" name="idUser" value="${usr.id}">
                    <div class="form-group">
                        <label for="com_game"><fmt:message key="com.select_game"/>:</label>
                        <select class="form-control" id="com_game" name="comGame">
                            <c:forEach var="game" items="${games}">
                                <option value="${game.id}"><fmt:message key="${game.title}"/></option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="com_left"><fmt:message key="com.left_title"/>:</label>
                        <input type="text" class="form-control" id="com_left" name="leftSide">
                    </div>
                    <div class="form-group">
                        <label for="com_right"><fmt:message key="com.right_title"/>:</label>
                        <input type="text" class="form-control" id="com_right" name="rightSide">
                    </div>

                    <div class="form-group">
                        <label for="com_left_g"><fmt:message key="com.left_goals"/>:</label>
                        <input type="number" class="form-control" id="com_left_g" name="leftSideGoal" value="0">
                    </div>
                    <div class="form-group">
                        <label for="com_right_g"><fmt:message key="com.right_goals"/>:</label>
                        <input type="number" class="form-control" id="com_right_g" name="rightSideGoal" value="0">
                    </div>
                    <div class="form-group">
                        <label for="com_date_s"><fmt:message key="com.datetime_start"/>:</label>
                        <input type="datetime-local" class="form-control" id="com_date_s" name="datetimeStart">
                    </div>
                    <div class="form-group">
                        <label for="com_date_e"><fmt:message key="com.datetime_end"/>:</label>
                        <input type="datetime-local" class="form-control" id="com_date_e" name="datetimeEnd">
                    </div>

                </div>
                <div class="modal-footer">
                    <div id="com_results"></div>
                    <img id="com_loading" class="loading" src="/images/loading.svg" alt="loading..."/>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message
                            key="close"/></button>
                    <button type="button" class="btn btn-primary" onclick="callCom();"><fmt:message
                            key="create"/></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function callCom() {
        $('#com_loading').show();
        var msg = $('#makeCompetitionForm').serialize();
        $.ajax({
            type: 'POST',
            url: '/',
            data: msg,
            success: function (data) {
                $('#com_results').html(data);
                if (data.indexOf("ok") != -1) {
                    $("#makeCompetitionModal").modal("hide");
                    //alert("<fmt:message key="seccessful_added"/>");
                    location.reload();
                }
                $('#com_loading').hide();
            },
            error: function (xhr, str) {
                alert("<fmt:message key="error.connect"/>");
                $('#com_loading').hide();
            }
        });

    }

</script>