<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 12.07.2017
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<!-- SIGN UP Modal -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="registerModalLabel"><fmt:message key="registration"/></h4>
            </div>
            <form class="registration-form" id="registerForm" action="/" method="post">
                <div class="modal-body">
                    <input type="hidden" name="command" value="sign_up">
                    <div class="field required">
                        <h4><fmt:message key="login"/>:</h4>
                        <input type="text" class="form-control" name="login"
                               placeholder="" id="id_username"
                               pattern="^([a-zA-Z]+)[a-zA-Z\d_]{4,}$"
                               title="" oninput="validateUserName();"
                               required/>
                        <h6><fmt:message key="login.requirements"/></h6>
                    </div>
                    <div class="field required">
                        <h4><fmt:message key="password"/>:</h4>
                        <input type="password" id="id_password" class="form-control" name="password"
                               placeholder=""
                               pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$"
                               title="" oninput="validatePassword();"
                               required/>
                    </div>
                    <div class="field required">
                        <h4><fmt:message key="confirm.password"/>:</h4>
                        <input type="password" id="id_confirm-password" class="form-control"
                               placeholder="" oninput="validatePassword();"
                               required/>
                        <h6><fmt:message key="password.requirements"/></h6>
                    </div>

                    <div class="field required email-container">
                        <h4><fmt:message key="email"/>:</h4>
                        <input type="email" class="form-control" name="email" id="id_email"
                               title="" oninput="validateEmail();"
                               placeholder="yourEmail@site.com" required/>
                        <h6><fmt:message key="email.requirements"/></h6>
                    </div>

                    <div class="field optional">
                        <h4><fmt:message key="first.name"/>:</h4>
                        <input type="text" class="form-control" name="first-name" oninput="validateFirstName();" id="id_first_name"
                               placeholder="">
                    </div>

                    <div class="field optional">
                        <h4><fmt:message key="last.name"/>:</h4>
                        <input type="text" class="form-control" name="last-name" oninput="validateLastName();" id="id_last_name"
                               placeholder="">
                    </div>

                </div>
                <div class="modal-footer">
                    <img id="register_loading" class="loading" src="/images/loading.svg" alt="loading..."/>
                    <ul id="register_errors"></ul>
                    <div id="register_results"></div>

                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message
                            key="close"/></button>
                    <button type="button" class="btn btn-primary" onclick="callRegister();"><fmt:message
                            key="signup.btn"/></button>
                </div>
            </form>


        </div>
    </div>
</div>
<script type="text/javascript" src="/js/registrationFormValidator.js"></script>
<script type="text/javascript">
    setLang("${sessionScope.locale}");
    function callRegister() {
        validateUserName();
        validatePassword();
        validateFirstName();
        validateLastName();
        validateEmail();
        if (ul.getElementsByTagName("li").length) {
            return false;
        }
        else {
            $('#register_loading').show();
            var msg = $('#registerForm').serialize();
            $.ajax({
                type: 'POST',
                url: '/',
                data: msg,
                success: function (data) {
                    $('#register_results').html(data);
                    if (data.indexOf("ok") != -1) {
                        //$("#registerModal").modal("hide");
                        alert("<fmt:message key="registration_seccessful"/>");
                        location.reload();
                    }
                    $('#register_loading').hide();
                },
                error: function (xhr, str) {
                    alert("<fmt:message key="error.connect"/>");
                    $('#register_loading').hide();
                }
            });
        }

    }

</script>