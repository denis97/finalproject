<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 12.07.2017
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<!-- edit Coefficients Modal -->
<div class="modal fade" id="editCoefficientsModal" tabindex="-1" role="dialog"
     aria-labelledby="editCoefficientsModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="editCoefficientsModalLabel"><fmt:message key="coeff.modalTitle"/></h4>
            </div>
            <form class="input-line" id="editCoefficientsForm" method="post">

                <div class="modal-body">
                    <input type="hidden" name="command" value="set_coefficients">
                    <input type="hidden" id="coeff_idCompetition" name="idCompetition" value="">

                    <div class="form-group">
                        <label for="coeff_1"><fmt:message key="coeff.leftWin"/>:</label>
                        <input type="number" class="form-control" id="coeff_1" name="coeff_1">
                        <label for="coeff_2"><fmt:message key="coeff.rightWin"/>:</label>
                        <input type="number" class="form-control" id="coeff_2" name="coeff_2">
                        <label for="coeff_x"><fmt:message key="coeff.exactRes"/>:</label>
                        <input type="number" class="form-control" id="coeff_x" name="coeff_X">
                    </div>


                </div>
                <div class="modal-footer">
                    <div id="coeff_results"></div>
                    <img id="coeff_loading" class="loading" src="/images/loading.svg" alt="loading..."/>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message
                            key="close"/></button>
                    <button type="button" class="btn btn-primary" onclick="callCoeff();"><fmt:message
                            key="save"/></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function callCoeff() {
        $('#coeff_loading').show();
        var msg = $('#editCoefficientsForm').serialize();
        $.ajax({
            type: 'POST',
            url: '/',
            data: msg,
            success: function (data) {
                $('#coeff_results').html(data);
                $('#coeff_loading').hide();
                if (data.indexOf("ok") != -1) {
                    $("#editCoefficientsModal").modal("hide");
                    //alert("<fmt:message key="seccessful"/>");
                    location.reload();
                }
            },
            error: function (xhr, str) {
                $('#coeff_loading').hide();
                alert("<fmt:message key="error.connect"/>");
            }
        });

    }
    function prepeareCoefficientForm(idCom, coefL, coefR, coefX) {


        $('#coeff_idCompetition').val(idCom);
        $('#coeff_1').val(coefL);
        $('#coeff_2').val(coefR);
        $('#coeff_x').val(coefX);
        $("#editCoefficientsModal").modal("show");

    }
</script>