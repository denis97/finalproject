<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 12.07.2017
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<!-- Make Com Modal -->
<div class="modal fade" id="editUserAdminModal" tabindex="-1" role="dialog" aria-labelledby="editUserAdminModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="editUserAdminModalLabel"><fmt:message key="user.edit"/></h4>
            </div>
            <form class="input-line" id="editUserAdminForm" method="post">

                <div class="modal-body">
                    <input type="hidden" name="command" value="edit_user_admin">
                    <input type="hidden" id="e_usr_iduser" name="idUser" value="">
                    <div class="form-group">
                        <label for="e_usr_role"><fmt:message key="role.title"/>:</label>
                        <select class="form-control" id="e_usr_role" name="role">
                            <option value="SIGNED_USER"><fmt:message key="role.user"/></option>
                            <option value="ADMIN"><fmt:message key="role.admin"/></option>
                            <option value="BOOKMAKER"><fmt:message key="role.bookmaker"/></option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="e_usr_fname"><fmt:message key="first.name"/>:</label>
                        <input type="text" class="form-control" id="e_usr_fname" name="firstName"
                               value="">

                        <label for="e_usr_lname"><fmt:message key="last.name"/>:</label>
                        <input type="text" class="form-control" id="e_usr_lname" name="lastName"
                               value="">

                        <label for="e_usr_email"><fmt:message key="email"/>:</label>
                        <input type="email" class="form-control" id="e_usr_email" name="email" value="">

                        <label for="e_usr_password"><fmt:message key="password"/>:</label>
                        <input type="text" class="form-control" id="e_usr_password" name="password" value=""
                               placeholder="<fmt:message key="user.password.change.placeholder"/>">

                        <label for="e_usr_money"><fmt:message key="money"/>:</label>
                        <input type="number" class="form-control" id="e_usr_money" name="money" value="">

                        <label for="e_usr_active"><fmt:message key="user.active"/>:</label>
                        <input type="checkbox" class="form-control" id="e_usr_active" name="banned">


                    </div>


                </div>
                <div class="modal-footer">
                    <div id="usr_e_results"></div>
                    <img id="usr_e_loading" class="loading" src="/images/loading.svg" alt="loading..."/>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message
                            key="close"/></button>
                    <button type="button" class="btn btn-primary" onclick="callUserEditAdmin();"><fmt:message
                            key="save"/></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function callUserEditAdmin() {
        $('#usr_e_loading').show();
        var msg = $('#editUserAdminForm').serialize();
        $.ajax({
            type: 'POST',
            url: '/',
            data: msg,
            success: function (data) {
                $('#usr_e_results').html(data);
                if (data.indexOf("ok") != -1) {
                    $("#editUserAdminModal").modal("hide");
                    //alert('<fmt:message key="seccessful_changed"/>');
                    $('#usr_e_loading').hide();
                    location.reload();
                }
            },
            error: function (xhr, str) {
                alert("<fmt:message key="error.connect"/>" + xhr.responseCode);
                $('#usr_e_loading').hide();
            }
        });

    }
    function prepeareEditUserAdminForm(idUser, firstName, lastName, email, password, active, money,role) {

        $('#e_usr_iduser').val(idUser);
        $('#e_usr_fname').val(firstName);
        $('#e_usr_lname').val(lastName);
        $('#e_usr_email').val(email);
        //$('#e_usr_password').val(password);
        $('#e_usr_active').prop('checked', active);
        $('#e_usr_money').val(money);
        $('#e_usr_role').val(role)
        $("#editUserAdminModal").modal("show");

    }


</script>