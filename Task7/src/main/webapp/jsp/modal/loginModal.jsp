<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 12.07.2017
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<!-- LOGIN Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="loginModalLabel"><fmt:message key="login.btn"/></h4>
            </div>
            <form class="input-line" id="loginForm" action="/" method="post">

                <div class="modal-body">
                    <input type="hidden" name="command" value="login">
                    <h3><fmt:message key="login"/>: </h3>
                    <input type="text" class="form-control" name="known-login"
                           placeholder=
                                   "" pattern="^([a-zA-Z]+)[a-zA-Z\d_]{4,}$"
                           title=""
                           required/>
                    <h3><fmt:message key="password"/>: </h3>
                    <input type="password" class="form-control" name="known-password"
                           placeholder=""
                           pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$"
                           title=""
                           required/>


                </div>
                <div class="modal-footer">
                    <img id="login_loading" class="loading" src="/images/loading.svg" alt="loading..."/>
                    <div id="login_results"></div>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message
                            key="close"/></button>
                    <button type="button" class="btn btn-primary" onclick="callLogin();"><fmt:message
                            key="login.btn"/></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function callLogin() {
        $('#login_loading').show();
        var msg = $('#loginForm').serialize();
        $.ajax({
            type: 'POST',
            url: '/',
            data: msg,
            success: function (data) {
                $('#login_results').html(data);
                if (data.indexOf("ok") != -1) {
                    //$("#loginModal").modal("hide");
                    location.reload();
                }
                $('#login_loading').hide();
            },
            error: function (xhr, str) {
                alert("<fmt:message key="error.connect"/>");
                $('#login_loading').hide();
            }
        });


    }

</script>