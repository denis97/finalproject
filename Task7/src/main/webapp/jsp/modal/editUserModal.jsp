<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 12.07.2017
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<!-- Make Com Modal -->
<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="editUserModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="editUserModalLabel"><fmt:message key="user.edit"/></h4>
            </div>
            <form class="input-line" id="editUserForm" method="post">

                <div class="modal-body">
                    <input type="hidden" name="command" value="edit_user">


                    <div class="form-group">
                        <label for="e_usr_fname"><fmt:message key="first.name"/>:</label>
                        <input type="text" class="form-control" id="e_usr_fname" name="firstName"
                               value="${usr.firstName}">

                        <label for="e_usr_lname"><fmt:message key="last.name"/>:</label>
                        <input type="text" class="form-control" id="e_usr_lname" name="lastName"
                               value="${usr.lastName}">

                        <label for="e_usr_email"><fmt:message key="email"/>:</label>
                        <input type="text" class="form-control" id="e_usr_email" name="email" value="${usr.email}">


                    </div>


                </div>
                <div class="modal-footer">
                    <div id="usr_e_results"></div>
                    <img id="usr_e_loading" class="loading" src="/images/loading.svg" alt="loading..."/>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message
                            key="close"/></button>
                    <button type="button" class="btn btn-primary" onclick="callUserEdit();"><fmt:message
                            key="save"/></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function callUserEdit() {
        $('#usr_e_loading').show();
        var msg = $('#editUserForm').serialize();
        $.ajax({
            type: 'POST',
            url: '/',
            data: msg,
            success: function (data) {
                $('#usr_e_results').html(data);
                if (data.indexOf("ok") != -1) {
                    $("#editUserModal").modal("hide");
                    //alert("<fmt:message key="seccessful_changed"/>");
                    $('#usr_e_loading').hide();
                    location.reload();
                }
            },
            error: function (xhr, str) {
                alert("<fmt:message key="error.connect"/>" + xhr.responseCode);
                $('#usr_e_loading').hide();
            }
        });

    }


</script>