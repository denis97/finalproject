<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 12.07.2017
  Time: 15:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<!-- Make Bet Modal -->
<div class="modal fade" id="makeBetModal" tabindex="-1" role="dialog" aria-labelledby="makeBetModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="makeBetModalLabel"><fmt:message key="bet.make"/></h4>
            </div>
            <form class="input-line" id="makeBetForm" method="post">

                <div class="modal-body">
                    <input type="hidden" name="command" value="make_bet">
                    <input type="hidden" id="bet_idCompetition" name="idCompetition" value="">
                    <input type="hidden" id="bet_idBetType" name="idBetType" value="">
                    <input type="hidden" id="bet_idUser" name="idUser" value="">
                    <h4 id="bet_betinfo">loading...</h4>
                    <div class="form-group">
                        <label for="bet_money"><fmt:message key="bet.money"/>:</label>
                        <input type="number" class="form-control" id="bet_money" name="money" value="1.00">
                    </div>
                    <div id="bet_instanceFields" class="form-group">
                        <label for="bet_left"><fmt:message key="bet.leftsideg"/>:</label>
                        <input type="number" class="form-control" id="bet_left" name="leftSideGoal" value="0">
                        <label for="bet_right"><fmt:message key="bet.rightsideg"/>:</label>
                        <input type="number" class="form-control" id="bet_right" name="rightSideGoal" value="0">
                    </div>


                </div>
                <div class="modal-footer">
                    <div id="bet_results"></div>
                    <img id="bet_loading" class="loading" src="/images/loading.svg" alt="loading..."/>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message
                            key="close"/></button>
                    <button type="button" class="btn btn-primary" onclick="call();"><fmt:message key="bet.make"/>!
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function call() {
        $('#bet_loading').show();
        var msg = $('#makeBetForm').serialize();
        $.ajax({
            type: 'POST',
            url: '/',
            data: msg,
            success: function (data) {
                $('#bet_results').html(data);
                $('#bet_loading').hide();
                if (data.indexOf("ok") != -1) {
                    $("#makeBetModal").modal("hide");
                    alert("<fmt:message key="seccessful"/>");
                }
            },
            error: function (xhr, str) {
                $('#bet_loading').hide();
                alert("<fmt:message key="error.connect"/>");
            }
        });

    }
    function prepeareBetForm(idCom, idBetType, idUser, betinfo) {
        if (idUser == -1) {
            $("#loginModal").modal("show");
        }
        else {
            $("#bet_results").html("");
            if (idBetType == 3) {
                $('#bet_instanceFields').show();
            } else {
                $('#bet_instanceFields').hide();
            }
            $('#bet_idBetType').val(idBetType);
            $('#bet_idCompetition').val(idCom);
            $('#bet_idUser').val(idUser);
            $('#bet_betinfo').html(betinfo);
            $("#makeBetModal").modal("show");
        }
    }
</script>