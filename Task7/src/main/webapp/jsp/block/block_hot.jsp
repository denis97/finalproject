<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 20.07.2017
  Time: 14:27
  To change this template use File | Settings | File Templates.
--%>
<fmt:setLocale value="${sessionScope.locale}"/>
<c:if test="${nextcompetition.id!=null}">
    <div class="center-hot-block">

        <div class="block-content">
            <img class="sport-icon" src="images/icons/sports/${nextcompetition.idGame}.svg" alt="sport icon"/>
            <div class="right-stick">
                <fmt:formatDate type="both" dateStyle="short" timeStyle="short"
                                value="${nextcompetition.datetimeStart}"/>

            </div>
            <c:out value="${nextcompetition.leftSide}"/>-<c:out value="${nextcompetition.rightSide}"/> (<fmt:message
                key="${nextcompetition.game.title}"/>)
            <c:forEach var="coef" items="${nextcompetition.coefficients}">
                <a class="coefficient" onclick="
                        prepeareBetForm(${nextcompetition.id},${coef.idCoefficientType},${usrid},'${nextcompetition.leftSide}-${nextcompetition.rightSide}(${coef.title}:${coef.coefficient})');"><c:out
                        value="${coef.title}"/>: <c:out
                        value="${coef.coefficient}"/></a>
            </c:forEach>

        </div>
    </div>
</c:if>