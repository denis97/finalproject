<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 20.07.2017
  Time: 14:27
  To change this template use File | Settings | File Templates.
--%>
<fmt:setLocale value="${sessionScope.locale}"/>
<div class="right-block">
    <div class="block-header"><h3><span id="liveCircle" style="color: red;"><i class="fa fa-circle"
                                                                               aria-hidden="true"></i></span>
        <fmt:message key="live"/></h3></div>
    <div class="block-content">
        <ul class="competitions" id="liveCompetitions">

            <li>
                <div class="competition" id="livetest">
                </div>
            </li>


        </ul>
    </div>
</div>

<script type="text/javascript">
    function liveUpdate() {

        $.ajax({
            type: 'POST',
            url: '/?command=get_live',
            success: function (data) {
                setLiveBlock(data);
                $('#liveCircle').fadeIn(500);
                setTimeout(function () {
                    $('#liveCircle').fadeOut(500);
                    liveUpdate();
                }, 10000);
            },
            error: function (xhr, str) {
                $('#liveCompetitions').html("<li><div class='competition'><fmt:message key="error.connect"/></div></li>");
            }
        });

    }
    function setLiveBlock(data) {
        livecompetitions = JSON.parse(data);
        $('#liveCompetitions').html("");
        if (livecompetitions.length == 0) {
            var result = "<li><div class='competition'><fmt:message key="live_nogames"/></div></li>";
            $('#liveCompetitions').append(result);
        }
        livecompetitions.forEach(function (item, i, arr) {
            var result = "<li><div class='competition'>" +
                "<img class='sport-icon' src='images/icons/sports/" + item.idGame + "-b.svg' alt='sport icon'/> " + item.leftSide + "-" + item.rightSide + " " +
                "<span class='coefficient'>" + item.leftSideGoal + "</span> " +
                "<b>-</b> <span class='coefficient'>" + item.rightSideGoal + "</span></div></li>";
            $('#liveCompetitions').append(result);
        });
    }
    liveUpdate();
</script>
