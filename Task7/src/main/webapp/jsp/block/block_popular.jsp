<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 20.07.2017
  Time: 14:27
  To change this template use File | Settings | File Templates.
--%>
<fmt:setLocale value="${sessionScope.locale}"/>
<div class="right-block">
    <div class="block-header"><h3><i class="fa fa-line-chart" aria-hidden="true"></i> <fmt:message key="popular"/></h3>
    </div>
    <div class="block-content">
        <ul class="competitions">
            <c:forEach var="com" items="${topcompetitions}">
                <li>
                    <div class="competition">
                        <div style="float:left; width: 60px; height: 100%;"><img class="sport-icon"
                                                                                 src="images/icons/sports/${com.idGame}-b.svg"
                                                                                 alt="sport icon"
                                                                                 style="margin-top:10px;"/></div>
                        <div style="margin-left: 65px;"><c:out value="${com.leftSide}"/>-<c:out
                                value="${com.rightSide}"/><br>
                            <c:forEach var="coef" items="${com.coefficients}">
                                <a class="coefficient" onclick="
                                        prepeareBetForm(${com.id},${coef.idCoefficientType},${usrid},'${com.leftSide}-${com.rightSide}(${coef.title}:${coef.coefficient})');"><c:out
                                        value="${coef.title}"/>: <c:out
                                        value="${coef.coefficient}"/></a>
                            </c:forEach>
                            <br><fmt:message key="time"/>: <fmt:formatDate type="both" dateStyle="short"
                                                                           timeStyle="short"
                                                                           value="${com.datetimeStart}"/></div>

                    </div>
                </li>
            </c:forEach>

        </ul>
    </div>
</div>