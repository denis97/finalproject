<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="my" uri="/WEB-INF/tld/myCustomTag" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 17.06.2017
  Time: 18:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><fmt:message key="user.manage.title"/> - Sport Bet Site</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="Description" content="Sports betting site.">
    <meta name="Keywords" content="sports betting, sport, ставки на спорт, тотализатор">

    <link rel="shortcut icon" href="images/favicon.png">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<c:import url="part/header.jsp"/>
<main>
    <div class="main__content">
        <h2 class="center-title"><fmt:message key="user.manage.title"/></h2>
        <div id="last-competitions">
            <p>
            <form method="get" action="/">
                <input type="hidden" value="find_user" name="command"/>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="input-group input-group-lg">
                <input type="text" class="form-control" name="search" placeholder="<fmt:message key="user.find.placeholder"/>"/>
                            <div class="input-group-btn">
                <input type="submit" class="btn btn-default" value="Search"/>
                            </div><!-- /btn-group -->
                        </div><!-- /input-group -->
                    </div><!-- /.col-xs-12 -->
                </div>
            </form>
            </p>
            <table class="competitions-table width-full">
                <tbody>

                <tr>
                    <th scope="col"><fmt:message key="user.manage.loginandemail"/>, <fmt:message key="user.manage.name"/></th>
                    <th scope="col"><fmt:message key="user.manage.account"/></th>
                </tr>
                <c:forEach var="myuser" items="${list}">
                    <tr>
                        <td>
                            ${myuser.login} (${myuser.email}), <br/>${myuser.firstName} ${myuser.lastName}
                        </td>
                        <td>
                            <fmt:message key="money"/>: <my:moneyint value="${myuser.money}"/><i class="fa fa-usd" aria-hidden="true"></i> <br/>
                            <fmt:message key="user.active"/>: <c:choose>
                            <c:when test="${myuser.active}"><i class="fa fa-check-circle-o" aria-hidden="true"></i></c:when>
                            <c:otherwise><i class="fa fa-ban" aria-hidden="true"></i></c:otherwise>
                        </c:choose><br/>
                            <a onclick="prepeareEditUserAdminForm(${myuser.id},'${myuser.firstName}','${myuser.lastName}','${myuser.email}','${myuser.password}',${myuser.active},${myuser.money},'${myuser.role}')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>


                </c:forEach>
                </tbody>
            </table>
            <c:import url="part/paginator.jsp"/>
        </div>
    </div>
    <c:import url="part/menu_left.jsp"/>


</main>

<c:import url="part/footer.jsp"/>
<c:import url="modal/editUserAdminModal.jsp"/>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>

</html>


