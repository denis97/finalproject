<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 09.07.2017
  Time: 18:48
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="/WEB-INF/tld/myCustomTag" prefix="my" %>
<%--
  Created by IntelliJ IDEA.
  User: Denis
  Date: 17.06.2017
  Time: 18:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><fmt:message key="profile"/> - Sport Bet Site</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="Description" content="Sports betting site.">
    <meta name="Keywords" content="sports betting, sport, ставки на спорт, тотализатор">

    <link rel="shortcut icon" href="images/favicon.png">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<c:import url="part/header.jsp"/>
<main>
    <div class="main__content">

        <h2 class="center-title"><fmt:message key="mybets"/></h2>
        <br>

        <c:choose>
            <c:when test="${empty list}">
                <h3 class="center-title"><fmt:message key="no_bets"/></h3>
            </c:when>
            <c:otherwise>
                <div id="last-competitions">
                    <table class="competitions-table large-only">
                        <tbody>

                        <tr>
                            <th scope="col"><fmt:message key="game"/></th>
                            <th scope="col"><fmt:message key="com"/></th>
                            <th scope="col"><fmt:message key="com.end_time"/></th>
                            <th scope="col"><fmt:message key="bet_time"/></th>
                            <th scope="col"><fmt:message key="bet_type"/></th>
                            <th scope="col"><fmt:message key="game_result"/></th>
                            <th scope="col"><fmt:message key="money_win"/></th>
                        </tr>
                        <c:forEach var="bet" items="${list}">
                            <tr>
                                <td><img class="sport-icon" src="images/icons/sports/${bet.competition.idGame}-b.svg"
                                         alt="sport icon"/> <fmt:message key="${bet.competition.game.title}"/></td>
                                <td>
                                    <div class="competition"><c:out value="${bet.competition.leftSide}"/>-<c:out
                                            value="${bet.competition.rightSide}"/>
                                        <br>
                                        <c:forEach var="coef" items="${bet.competition.coefficients}">
                                            <a href="#" class="coefficient"><c:out value="${coef.title}"/>: <c:out
                                                    value="${coef.coefficient}"/></a>
                                        </c:forEach>

                                    </div>
                                </td>
                                <td><fmt:formatDate type="both" dateStyle="long" timeStyle="short"
                                                    value="${bet.competition.datetimeEnd}"/></td>
                                <td><fmt:formatDate type="both" dateStyle="long" timeStyle="short"
                                                    value="${bet.datetime}"/></td>
                                <td><fmt:message key="bet_type"/>:
                                    <c:choose>
                                        <c:when test="${bet.idBetType==3}">
                                            <fmt:message key="exact_result"/> (<c:out value="${bet.leftSideGoal}"/>-<c:out
                                                value="${bet.rightSideGoal}"/>)
                                        </c:when>
                                        <c:when test="${bet.idBetType==1}">
                                            <c:out value="${bet.competition.leftSide}"/> <fmt:message key="win"/>
                                        </c:when>
                                        <c:when test="${bet.idBetType==2}">
                                            <c:out value="${bet.competition.rightSide}"/> <fmt:message key="win"/>
                                        </c:when>
                                        <c:otherwise>
                                            error
                                        </c:otherwise>
                                    </c:choose>

                                    <br><fmt:message key="money"/>: <my:moneyint value="${bet.money}"/>USD
                                    * ${bet.coefficient.coefficient}</td>
                                <c:choose>
                                    <c:when test="${bet.competition.datetimeEnd<datetime_now}">
                                        <td><c:out value="${bet.competition.leftSideGoal}"/>-<c:out
                                                value="${bet.competition.rightSideGoal}"/></td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${bet.processed}">
                                                    <c:choose>
                                                        <c:when test="${bet.win}">
                                                            <fmt:message
                                                                    key="you_win_money"/>: ${(bet.money/100)*bet.coefficient.coefficient} USD
                                                        </c:when>
                                                        <c:otherwise>
                                                            <fmt:message key="not_win"/>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:when>
                                                <c:otherwise>
                                                    <fmt:message key="bet_not_processed"/>
                                                </c:otherwise>
                                            </c:choose>

                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td><c:out value="${bet.competition.leftSideGoal}"/>-<c:out
                                                value="${bet.competition.rightSideGoal}"/><br><fmt:message key="game_not_end"/>!
                                        </td>
                                        <td><fmt:message key="game_not_end"/></td>
                                    </c:otherwise>
                                </c:choose>

                            </tr>


                        </c:forEach>


                        </tbody>
                    </table>

                    <div id="bets-mobile" class="mobile-only">
                        <c:forEach var="bet" items="${list}">
                            <div class="right-block">
                                <div class="block-header"><h3><img class="sport-icon"
                                                                   src="images/icons/sports/${bet.competition.idGame}.svg"
                                                                   alt="sport icon"/> <fmt:message
                                        key="${bet.competition.game.title}"/></h3>
                                </div>
                                <div class="block-content">
                                    <div class="competition"><b><fmt:message key="com"/>: </b> <c:out
                                            value="${bet.competition.leftSide}"/>-<c:out
                                            value="${bet.competition.rightSide}"/>
                                        <br/>
                                        <c:forEach var="coef" items="${bet.competition.coefficients}">
                                            <a href="#" class="coefficient"><c:out value="${coef.title}"/>: <c:out
                                                    value="${coef.coefficient}"/></a>
                                        </c:forEach>

                                        <br/>
                                        <b><fmt:message key="com.end_time"/>: </b><fmt:formatDate type="both"
                                                                                                 dateStyle="long"
                                                                                                 timeStyle="short"
                                                                                                 value="${bet.competition.datetimeEnd}"/><br/>
                                        <b><fmt:message key="bet_time"/>: </b><fmt:formatDate type="both"
                                                                                             dateStyle="long"
                                                                                             timeStyle="short"
                                                                                             value="${bet.datetime}"/><br/>
                                        <b><fmt:message key="bet_type"/>: </b>
                                        <c:choose>
                                            <c:when test="${bet.idBetType==3}">
                                                <fmt:message key="exact_result"/> (<c:out value="${bet.leftSideGoal}"/>-<c:out
                                                    value="${bet.rightSideGoal}"/>)
                                            </c:when>
                                            <c:when test="${bet.idBetType==1}">
                                                <c:out value="${bet.competition.leftSide}"/> <fmt:message key="win"/>
                                            </c:when>
                                            <c:when test="${bet.idBetType==2}">
                                                <c:out value="${bet.competition.rightSide}"/> <fmt:message key="win"/>
                                            </c:when>
                                            <c:otherwise>
                                                error
                                            </c:otherwise>
                                        </c:choose>

                                        <br><b><fmt:message key="money"/>: </b> <my:moneyint value="${bet.money}"/>USD
                                        * ${bet.coefficient.coefficient}<br/>
                                        <c:choose>
                                            <c:when test="${bet.competition.datetimeEnd<datetime_now}">
                                                <b><fmt:message key="game_result"/>: </b><c:out
                                                    value="${bet.competition.leftSideGoal}"/>-<c:out
                                                    value="${bet.competition.rightSideGoal}"/><br/>
                                                <b><fmt:message key="money_win"/>: </b>
                                                <c:choose>
                                                    <c:when test="${bet.processed}">
                                                        <c:choose>
                                                            <c:when test="${bet.win}">
                                                                <fmt:message
                                                                        key="you_win_money"/>: ${(bet.money/100)*bet.coefficient.coefficient} USD
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="not_win"/>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <fmt:message key="bet_not_processed"/>
                                                    </c:otherwise>
                                                </c:choose>

                                                <br/>
                                            </c:when>
                                            <c:otherwise>
                                                <b><fmt:message key="game_result"/>: </b><c:out
                                                    value="${bet.competition.leftSideGoal}"/>-<c:out
                                                    value="${bet.competition.rightSideGoal}"/><br><fmt:message
                                                    key="game_not_end"/>!
                                                <br/>
                                                <b><fmt:message key="money_win"/>: </b><fmt:message
                                                    key="game_not_end"/><br/>
                                            </c:otherwise>
                                        </c:choose>

                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <c:import url="part/paginator.jsp"/>
                </div>
            </c:otherwise>
        </c:choose>

    </div>
    <div class="main__sidebar main__sidebar--first">
        <div id="sportsnav">
            <div class="menu-wrapper">
                <c:import url="/jsp/part/mobile_menu.jsp"/>
                <ul class="left-menu">
                    <li class="title"><fmt:message key="profile"/></li>
                    <li><br/>
                        <div id="profile" class="clearfix">
                            <img style="max-width: 150px;" class="img-circle center-block"
                                 src="/images/avatar/${usr.avatar}" alt="user avatar"/>
                            <div class="title">
                                <h2>${usr.firstName} ${usr.lastName}</h2>
                                <h3>${usr.login} (<my:moneyint value="${usr.money}"/> USD)</h3>
                            </div>

                        </div>
                    </li>
                    <li><a><span class="left-menu-item"><i class="fa fa-usd" aria-hidden="true"></i> <fmt:message
                            key="top_up_balance"/></span></a>
                    </li>
                    <li><a><span class="left-menu-item"><i class="fa fa-money" aria-hidden="true"></i> <fmt:message
                            key="top_down_balance"/></span></a>
                    </li>
                    <li><a href="#" data-toggle="modal" data-target="#editUserModal"><span class="left-menu-item"><i
                            class="fa fa-pencil-square-o" aria-hidden="true"></i> <fmt:message
                            key="edit_profile"/></span></a>
                    </li>

                </ul>
            </div>

        </div>
    </div>


</main>
<c:import url="part/footer.jsp"/>


<c:import url="modal/editUserModal.jsp"/>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>

</html>



